#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the csp-lmc-prototype project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
"""Contain the tests for the CspSubarray."""

# Standard imports
import sys
import os
import time
import random
import numpy as np
import logging
import pytest
import json

# Tango imports
import tango
from tango import DevState
from assertpy import assert_that

#Local imports
from ska.base.control_model  import ObsState
from unit.utils import Probe, Poller

LOGGER = logging.getLogger(__name__)
file_path = os.path.dirname(os.path.abspath(__file__))

def prepare_configuration_string(filename="test_ConfigureScan_ADR4.json"):
    """Create the config string for CSP-CBF"""
    try:
        file_to_load = file_path + '/../test_data/' + filename
        LOGGER.info(f" Reading configuration from {file_to_load}")
        json_file = open(file_to_load)
        configuration_string = json_file.read().replace("\n", "")
        return configuration_string
    except Exception as e:
        LOGGER.warning(f"Unable to locate file {filename}")

# Device test case
@pytest.mark.usefixtures("midcsp_master", "midcsp_subarray01", "cbf_subarray01")
class TestBase(object):
    fixture_names = ()

    @pytest.fixture(autouse=True)
    def auto_injector_fixture(self, request):
        names = self.fixture_names
        for name in names:
            setattr(self, name, request.getfixturevalue(name))

class TestCspSubarray(TestBase):
    fixture_names = ("midcsp_master", "midcsp_subarray01", "cbf_subarray01")

    def _setup_subarray(self):
        """
        Set the subarray state to ON-EMPTY
        """
        subarray_obs_state = self.midcsp_subarray01.obsState
        subarray_state = self.midcsp_subarray01.State()
        LOGGER.info(f"Init subarray state:{subarray_state}-{ObsState(subarray_obs_state).name}")
        if subarray_state == DevState.FAULT:
            self.midcsp_subarray01.Reset()
            subarray_state = self.midcsp_subarray01.State()
        self.midcsp_subarray01.Off()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_state)
        master_state = self.midcsp_master.State()
        LOGGER.info(f"Init master state:{master_state}")
        if master_state == DevState.STANDBY:
            argin =["mid_csp_cbf/sub_elt/master",]
            self.midcsp_master.On(argin)
            prober_master_state = Probe(self.midcsp_master, "State", DevState.ON, f"CSP Master not ON")
            Poller(4, 0.2).check(prober_master_state)
            prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Master not ON")
            Poller(4, 0.2).check(prober_subarray_state)
        subarray_state = self.midcsp_subarray01.State()
        subarray_obs_state = self.midcsp_subarray01.obsState
        if subarray_state == DevState.OFF:
            self.midcsp_subarray01.On()
            prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Master not ON")
            Poller(4, 0.2).check(prober_subarray_state)
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        if subarray_state == DevState.ON:
            if obs_state == ObsState.EMPTY:
                return
            self.midcsp_subarray01.Off()
            prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
            Poller(4, 0.2).check(prober_subarray_state)
            prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
            Poller(4, 0.2).check(prober_subarray_obsstate)
            self.midcsp_subarray01.On()
            prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON")
            Poller(4, 0.2).check(prober_subarray_state)

    def _setup_subarray_off(self):
        """
        Set the subarray state to OFF-EMPTY
        """
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Master not ON")
        Poller(4, 0.2).check(prober_subarray_state)

    def _assign_receptors(self, value ='all'):
        """
        Assign a random number of available receptors to the subarray.
        If 'all' is specified (default) all receptors are assigned.
        The final subarray state is ON-IDLE
        """
        receptor_list = self.midcsp_master.unassignedReceptorIDs
        LOGGER.info(f"Available receptors: {receptor_list}")
        # assert the array is not empty
        assert receptor_list.any()
        # assign all available receptors to the subarray
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.debug("CSP Subarray state :{}-{}".format(state, ObsState(obs_state).name))
        receptor_list = receptor_list.tolist()
        random.seed()
        if value != 'all':
            num_of_receptors_to_assign = random.randrange(1, 4, 1)
            LOGGER.info(f"number of receptors: {num_of_receptors_to_assign}")
            tmp_list = random.sample(set(receptor_list), k=num_of_receptors_to_assign)
            receptor_list = tmp_list
        LOGGER.info(f"Going to assign receptors {receptor_list} to CSP Subarray")
        param = {
            'subarrayID': 1,
            'dish': {'receptorIDList': list(map(str, receptor_list))}}
        json_config = json.dumps(param)
        self.midcsp_subarray01.AssignResources(json_config)

        # wait for the transition to IDLE
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.IDLE,
                                           f"Wrong CSP Subarray obsState is not IDLE")
        Poller(10, 0.2).check(prober_obs_state)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert len(receptors) > 0

    def _release_all_receptors(self):
        """
        Remove all the receptors from the subarray.
        The final subarray state is ON-EMPTY
        """
        obs_state = self.midcsp_subarray01.obsState
        assert obs_state == ObsState.IDLE
        receptors = self.midcsp_subarray01.assignedReceptors
        LOGGER.info("Receptor assigned to the subarray {}".format(receptors))
        assert receptors.any()
        try:
            LOGGER.info("invoke remove all receptors")
            self.midcsp_subarray01.ReleaseAllResources()
        except Exception as e:
            LOGGER.info(str(e))
        # wait for the transition to EMPTY
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray is not EMPTY")
        Poller(4, 0.2).check(prober_obs_state)
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("obs_state:{}".format(obs_state))
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any()
        LOGGER.info("Receptor assigned to the subarray {}".format(receptors))

    def _goto_idle(self):
        """
        Force the subarray transition from READY to IDLE.
        """
        obs_state = self.midcsp_subarray01.obsState
        assert obs_state == ObsState.READY
        self.midcsp_subarray01.GoToIdle()
        # wait for the transition to IDLE
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.IDLE, f"CSP Subarray is not IDLE")
        Poller(4, 0.2).check(prober_obs_state)

    def _configure_scan(self):
        """
        Configure a subarray sca using the default configuration file.
        """
        self._setup_subarray()
        self._assign_receptors()
        configuration_string = prepare_configuration_string()
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        prober_subarray_obstate = Probe(self.midcsp_subarray01, 'obsState', ObsState.READY,
                                        f"Wrong CSP Subarray obsState {self.midcsp_subarray01.obsState}")
        Poller(5, 0.2).check(prober_subarray_obstate)

    def _build_resources(self, argin):
        """
        Prepare the json string with the resources to allocate
        """
        param = {
            'subarrayID': 1,
            'dish': { 'receptorIDList': list(map(str, argin))}}
        return json.dumps(param)


    @pytest.mark.transaction_id_in_CBF
    def test_transaction_id_is_present_in_CBF_configuration(self):
        """
        Test if the transaction id is properly sent to CBF
        """
        self._configure_scan()
        latest_configuration = self.cbf_subarray01.latestscanconfig
        assert 'transaction_id' in latest_configuration

    @pytest.mark.init_EMPTY
    def test_AFTER_initialization(self):
        """
        Test for State after CSP startup.
        The CspSubarray State at start is OFF.
        """
        self._setup_subarray_off()
        state = self.midcsp_subarray01.State()
        LOGGER.info("subarray state:{}".format(state))
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
   
    @pytest.mark.init_IDLE
    @pytest.mark.skip(reason="DevRestart doesn't work properly")
    def test_re_initialization_when_IDLE(self):
        """
        Test for re-initialization of mid-csp device after a restart of
        the Tango device. 
        The CspSubarray aligns to the state/obsstate of CBFSubarray: ON/IDLE
        """
        self._setup_subarray()
        json_config= self._build_resources([1,2])
        self.midcsp_subarray01.AssignResources(json_config)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.IDLE, f"CSP Subarray not IDLE")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        # open a proxy to the adminstrative server
        dserver_proxy = tango.DeviceProxy(self.midcsp_subarray01.adm_name())
        dserver_proxy. devrestart('mid_csp/elt/subarray_01')
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON after re-initialization")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.IDLE, f"CSP Subarray not IDLE after re-initialization")
        Poller(4, 0.2).check(prober_subarray_obsstate)

    @pytest.mark.init_READY
    @pytest.mark.skip(reason="DevRestart doesn't work properly")
    def test_re_initialization_when_READY(self):
        """
        Test for re-initialization of mid-csp device after the restart of
        the Tango device. 
        The CspSubarray aligns to the state/obsstate of CBFSubarray: ON/READY
        """
        self._setup_subarray()
        self._configure_scan() 
        # open a proxy to the adminstrative server
        dserver_proxy = tango.DeviceProxy(self.midcsp_subarray01.adm_name())
        dserver_proxy. devrestart('mid_csp/elt/subarray_01')
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON after re-initialization")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f"CSP Subarray not READY after re-initialization")
        Poller(4, 0.2).check(prober_subarray_obsstate)
    
    @pytest.mark.init_SCANNING
    @pytest.mark.parametrize("elapsed_time", [0.1, 0.2, 0.3, 0.4, 0.5,1,2])
    @pytest.mark.skip(reason="DevRestart doesn't work properly")
    def test_re_initialization_when_SCANNING(self, elapsed_time):
        """
        Test for re-initialization of mid-csp device after the of the
        Tango device.
        The CspSubarray aligns to the state/obsstate of CBFSubarray: ON/SCANNING.
        The tests ends with the CSP Subarray in READY.
        Test is executed several times with different wait time (dealys) before
        sending the EndScan.
        """
        self._setup_subarray()
        self._configure_scan()
        self.midcsp_subarray01.Scan('2')
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.SCANNING, f'CSP Subarray not SCANNING')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        # wait for a variable time
        time.sleep(elapsed_time)
        # open a proxy to the adminstrative server
        dserver_proxy = tango.DeviceProxy(self.midcsp_subarray01.adm_name())
        # restart the device to force a reinitialization
        dserver_proxy. devrestart('mid_csp/elt/subarray_01')
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON after re-initialization")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.SCANNING, f"CSP Subarray not SCANNING after re-initialization")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        # wait a variable time before sending the endscan
        time.sleep(elapsed_time)
        self.midcsp_subarray01.EndScan()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f'CSP Subarray not SCANNING')
        Poller(5, 0.2).check(prober_subarray_obsstate)
    
    @pytest.mark.init_ABORTED
    @pytest.mark.skip(reason="DevRestart doesn't work properly")
    def test_re_initialization_when_ABORTED(self):
        """
        Test for re-initialization of mid-csp device after the restart of the
        Tango device.
        The CspSubarray align to the state/obsstate of CBFSubarray: ON/ABORTED
        """
        self._setup_subarray()
        self._assign_receptors()
        self.midcsp_subarray01.Abort()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.ABORTED, f'CSP Subarray not ABORTED')        
        # open a proxy to the adminstrative server
        dserver_proxy = tango.DeviceProxy(self.midcsp_subarray01.adm_name())
        dserver_proxy. devrestart('mid_csp/elt/subarray_01')
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON after re-initialization")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.ABORTED, f"CSP Subarray not ABORTED after re-initialization")
        Poller(4, 0.2).check(prober_subarray_obsstate)
   
    @pytest.mark.init_FAULT
    @pytest.mark.skip(reason="DevRestart doesn't work properly")
    def test_re_initialization_when_FAULT(self):
        """
        Test for re-initialization of mid-csp device after killing the Tango device.
        The CspSubarray align to the state/obsstate of CBFSubarray: ON/IDLE
        """
        self._setup_subarray()
        self._assign_receptors()
        configuration_string = prepare_configuration_string("test_ConfigureScan_invalid_cbf_json.json")
        self.midcsp_subarray01.Configure(configuration_string)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.FAULT, f'CSP Subarray not FAULT')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        # open a proxy to the adminstrative server
        dserver_proxy = tango.DeviceProxy(self.midcsp_subarray01.adm_name())
        dserver_proxy. devrestart('mid_csp/elt/subarray_01')
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not ON after re-initialization")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.FAULT, f"CSP Subarray not FAULT after re-initialization")
        Poller(4, 0.2).check(prober_subarray_obsstate)
    
    def test_subarray_state_AFTER_on_command_execution(self):
        """
        Test for State after CSP startup.
        The CspSubarray State at start is OFF.
        """
        self._setup_subarray_off()
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.debug("CSPSubarray state before test:{}-{}".format(subarray_state, obs_state))
        subarray_state = self.midcsp_subarray01.State()
        self.midcsp_subarray01.On()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.ON, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)

    @pytest.mark.off_empty
    def test_OffCommand_after_EMPTY(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-EMPTY
        """
        self._setup_subarray()
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSPSubarray state before test:{}-{}".format(subarray_state, obs_state))
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        LOGGER.info(f'type of {type(receptors)}')
        LOGGER.info(f'list of receptors{receptors}')
        assert not receptors.any(), f"CSP Subarray is not empty"

    @pytest.mark.off_resourcing
    def test_OffCommand_after_RESOURCING(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-RESOURCING
        """
        self._setup_subarray()
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSPSubarray state before test:{}-{}".format(subarray_state, obs_state))
        json_config= self._build_resources([1,2])
        self.midcsp_subarray01.AssignResources(json_config)
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSPSubarray state before test:{}-{}".format(subarray_state, obs_state))
        self.midcsp_subarray01.Off()        
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        LOGGER.info(f'list of receptors{receptors}')
        assert not receptors.any(), f"CSP Subarray is not empty"

    def test_OffCommand_after_IDLE(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-IDLE
        """
        self._setup_subarray()
        self._assign_receptors()
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"

    def test_OffCommand_after_CONFIGURING(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-CONFIGURING
        """
        self._setup_subarray()
        self._configure_scan()
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(10, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"

    def test_OffCommand_after_READY(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-READY
        """
        self._setup_subarray()
        self._configure_scan()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f'CSP Subarray not READY')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(10, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"
   
    @pytest.mark.off
    def test_OffCommand_after_SCANNING(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-SCANNING
        """
        self._setup_subarray()
        self._configure_scan()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f'CSP Subarray not READY')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        self.midcsp_subarray01.Scan('2')
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.SCANNING, f'CSP Subarray not SCANNING')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(10, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(10, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"

    def test_OffCommand_after_ABORTED(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-ABORTED
        """
        self._setup_subarray()
        self._assign_receptors()
        self.midcsp_subarray01.Abort()
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.ABORTED, f'CSP Subarray not ABORTED')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"

    def test_OffCommand_after_FAULT(self):
        """
        Test that the Off command send the device in OFF-EMPTY
        if it is ON-FAULT
        """
        self._setup_subarray()
        self._assign_receptors()
        self.midcsp_subarray01.Configure('{"input":"failed"}')
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.FAULT, f'CSP Subarray not FAULT')
        Poller(10, 0.2).check(prober_subarray_obsstate)
        self.midcsp_subarray01.Off()
        prober_subarray_state = Probe(self.midcsp_subarray01, "State", DevState.OFF, f"CSP Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        prober_subarray_state = Probe(self.cbf_subarray01, "State", DevState.OFF, f"CBF Subarray not OFF")
        Poller(4, 0.2).check(prober_subarray_state)
        prober_subarray_obsstate = Probe(self.cbf_subarray01, "obsState", ObsState.EMPTY, f"CBF Subarray not EMPTY")
        Poller(4, 0.2).check(prober_subarray_obsstate)
        receptors = self.midcsp_subarray01.assignedReceptors
        assert not receptors.any(), f"CSP Subarray is not empty"
        
    def test_add_receptors_WITH_invalid_id(self):
        """
        Test the assignment of a number of invalid receptor IDs to
        a CspSubarray.
        The AddReceptors method fails raising a tango.DevFailed exception.
        """
        self._setup_subarray()
        receptors_list = self.midcsp_master.unassignedReceptorIDs
        LOGGER.info(f"list of receptors: {receptors_list}")
        # receptor_list is a numpy array
        # all(): test whether all array elements evaluate to True.
        assert receptors_list.all(), f"No available receptors to add to the subarray"
        invalid_receptor_to_assign = []
        # try to add 3 invalid receptors
        for id_num in range(190, 198):
            if id_num not in receptors_list:
                invalid_receptor_to_assign.append(id_num)
            if len(invalid_receptor_to_assign) > 3:
                break
        param = {
            'subarrayID': 1,
            'dish': { 'receptorIDList': list(map(str, invalid_receptor_to_assign))}}
        json_config = json.dumps(param)
        self.midcsp_subarray01.AssignResources(json_config)

        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray is not EMPTY")
        Poller(4, 0.2).check(prober_obs_state)
        receptors = self.midcsp_subarray01.assignedReceptors
        # receptors is a numpy array. In this test the returned array has to be
        # empty (no receptor assigned)
        #
        # Note:
        # any returns True if any value is True. Otherwise False
        # all returns True if no value is False. Otherwise True
        # In the case of np.array([]).any() or any([]), there are no True values, because you have
        # a 0-dimensional array or a 0-length list. Therefore, the result is False.
        # In the case of np.array([]).all() or all([]), there are no False values, because you have
        # a 0-dimensional array or a 0-length list. Therefore, the result is True.
        assert not receptors.any(), f"CSP Subarray is not empty"

    @pytest.mark.csp_k8s
    def test_add_receptors_WITH_valid_id(self):
        """
        Test the assignment of valid receptors to a CspSubarray
        """
        # Setup the system in ON-EMPTY state
        self._setup_subarray()
        # get the list of available receptorIDs (the read operation
        # returns a numpy array)
        receptor_list = self.midcsp_master.unassignedReceptorIDs
        # assert the array is not empty
        assert receptor_list.any()
        # Exercise the system: issue the AddREceptors command
        param = {
            'subarrayID': 1,
            'dish': {'receptorIDList': list(map(str, receptor_list))}}
        json_config = json.dumps(param)
        self.midcsp_subarray01.AssignResources(json_config)
        # check the final subarray obstate
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.IDLE,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)
        # read the list of assigned receptors
        receptors = self.midcsp_subarray01.assignedReceptors
        assert set(receptor_list) == set(receptors)

    @pytest.mark.csp_k8s
    def test_add_receptors_ALREADY_belonging_to_subarray(self):
        """
        Test the assignment of already assigned receptors to a CspSubarray
        """
        # Setup the system
        self._setup_subarray()
        self._assign_receptors(value='notall')
        # get the list of receptors belonging to the subarray
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        receptors_to_reassign = assigned_receptors
        LOGGER.info(f"receptors belonging to subarray:{assigned_receptors}")
        if len(assigned_receptors) > 1:
            num_of_receptors_to_reassign = random.randrange(1, len(assigned_receptors), 1)
            LOGGER.info(f"Going to reassign {num_of_receptors_to_reassign}")
            receptors_to_reassign = random.sample(set(assigned_receptors), k=num_of_receptors_to_reassign)
        # Exercise: try to re-assign one of the subarray receptors
        LOGGER.info(f"Try to assign receptor:{receptors_to_reassign}")
        param = {
            'subarrayID': 1,
            'dish': {'receptorIDList': list(map(str, receptors_to_reassign))}}
        json_config = json.dumps(param)
        self.midcsp_subarray01.AssignResources(json_config)
        # check 
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.IDLE,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)
        receptors = self.midcsp_subarray01.assignedReceptors
        # check the array read first and the array read last are equal
        assert np.array_equal(receptors, assigned_receptors)

    @pytest.mark.csp_k8s
    def test_subarray_state_AFTER_receptors_assignment(self):
        """
        Test the CspSubarray State after receptors assignment.
        After assignment State is ON
        """
        # read the list of assigned receptors and check it's not
        # empty
        self._setup_subarray()
        subarray_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.debug("CSPSubarray state before test:{}-{}".format(subarray_state, obs_state))
        self._assign_receptors()
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.IDLE,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)

    @pytest.mark.csp_k8s
    def test_partial_remove_of_receptors_FROM_the_subarray(self):
        """
        Test the partial deallocation of receptors from a
        CspSubarray.
        """

        # Setup the system
        self._setup_subarray()
        self._assign_receptors(value='notall')
        obs_state = self.midcsp_subarray01.obsState
        state = self.midcsp_subarray01.State()
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        random.seed()
        num_of_receptors_to_remove = 1 
        receptors_to_remove = assigned_receptors
        if len(assigned_receptors) > 1:
            num_of_receptors_to_remove = random.randrange(0, len(assigned_receptors))
            #receptors_to_remove = random.choices(assigned_receptors, k=num_of_receptors_to_remove)
            receptors_to_remove = random.sample(set(assigned_receptors.tolist()), k=num_of_receptors_to_remove)
        LOGGER.info(f"Going to remove the receptor(s) {receptors_to_remove} from the CSP Subarray")
        expected_receptors = set(assigned_receptors.tolist()) - set(receptors_to_remove)
        LOGGER.info(f"Expected remaining receptors {expected_receptors}")
        # Exercise the system: remove only one receptor (with a random ID)
        param = {
            'subarrayID': 1,
            'dish': {'receptorIDList': list(map(str, receptors_to_remove))}}
        json_config = json.dumps(param)
        self.midcsp_subarray01.ReleaseResources(json_config)
        target_obs_state = ObsState.EMPTY
        if len(expected_receptors) :
            target_obs_state = ObsState.IDLE
        # check 
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', target_obs_state,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.1).check(prober_obs_state)
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        if assigned_receptors.all():
            assert set(assigned_receptors.tolist()) == expected_receptors

    @pytest.mark.csp_k8s
    def test_remove_all_receptors_FROM_subarray(self):
        """
        Test the complete deallocation of receptors from a
        CspSubarray.
        Final CspSubarray state is OFF
        """
        self._setup_subarray()
        self._assign_receptors(value='notall')
        obs_state = self.midcsp_subarray01.obsState
        state = self.midcsp_subarray01.State()
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        # read the list of assigned receptors and check it's not
        # empty
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        LOGGER.info(f"Assigned receptors :{assigned_receptors}")
        assert assigned_receptors.any()
        LOGGER.info(f"Remove all receptors from CSP subarray01")
        self.midcsp_subarray01.ReleaseAllResources()
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.EMPTY,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)
        assigned_receptors = self.midcsp_subarray01.assignedReceptors
        # check the array is empty (any() in this case returns False)
        assert not assigned_receptors.any()

    @pytest.mark.csp_k8s
    def test_configure_WHEN_subarray_is_in_wrong_state(self):
        """
        Test that the Configure() command fails if the Subarray
        state is  not ON
        """
        self._setup_subarray()
        obs_state = self.midcsp_subarray01.obsState
        state = self.midcsp_subarray01.State()
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        configuration_string = prepare_configuration_string("test_ConfigureScan_basic.json")
        with pytest.raises(tango.DevFailed) as df:
            (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        if df:
            err_msg = str(df.value.args[0].desc)
            LOGGER.error(err_msg)
        obs_state = self.midcsp_subarray01.obsState
        assert obs_state == ObsState.EMPTY, f"CSP Subarray obsState is not EMPTY"

    @pytest.mark.csp_k8s
    def test_configure_WHITH_wrong_configuration(self):
        """
        Test that the Configure() command fails if the Subarray
        state is  not ON
        """
        self._setup_subarray()
        self._assign_receptors()
        obs_state = self.midcsp_subarray01.obsState
        state = self.midcsp_subarray01.State()
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        configuration_string = prepare_configuration_string("test_ConfigureScan_without_configID.json")
        LOGGER.info(f"Configuring CSP subarray01")
        self.midcsp_subarray01.Configure(configuration_string)
        # check
        obs_state = self.midcsp_subarray01.obsState
        assert_that(obs_state).described_as("CSP Subarray obsState has wrong value ({obs_state}").is_equal_to(ObsState.FAULT)

    @pytest.mark.csp_k8s
    def test_send_configure_to_cbf_and_json_stored(self):
        """
        Configure the CSP Subarray with a JSon string including
        the new ADR4 fields.
        """
        self._setup_subarray()
        self._assign_receptors()
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        # exercise the device
        LOGGER.info(f"Configuring CSP subarray01")
        configuration_string = prepare_configuration_string()
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        # check
        prober_subarray_obstate = Probe(self.midcsp_subarray01, 'obsState', ObsState.READY,
                                        f"Wrong CSP Subarray obsState {self.midcsp_subarray01.obsState}")
        Poller(5, 0.2).check(prober_subarray_obstate)
        obs_state = self.midcsp_subarray01.obsState
        #json_dict = json.loads(configuration_string)
        #configID = json_dict["id"]
        #stored_id = self.midcsp_subarray01.configurationID
        #assert stored_id == configID

    @pytest.mark.csp_k8s
    def test_send_configure_WITH_ADR22_json(self):
        """
        Configure the CSP Subarray with a JSon string including
        the new ADR4 fields.
        """
        self._setup_subarray()
        self._assign_receptors()
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        # exercise the device
        LOGGER.info(f"Configuring CSP subarray01")
        configuration_string = prepare_configuration_string("test_ConfigureScan_ADR22.json")
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        # check
        prober_subarray_obstate = Probe(self.midcsp_subarray01, 'obsState', ObsState.READY,
                                        f"Wrong CSP Subarray obsState {self.midcsp_subarray01.obsState}")
        Poller(5, 0.2).check(prober_subarray_obstate)
        obs_state = self.midcsp_subarray01.obsState

    @pytest.mark.csp_k8s
    def test_start_end_scan(self):
        """
        Test that a subarray is able to process the
        Scan command when its ObsState is READY
        """
        self._configure_scan()
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        LOGGER.info("Issue the Scan command")
        self.midcsp_subarray01.Scan("11")
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.SCANNING,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("Issue the EndScan command")
        self.midcsp_subarray01.EndScan()
        #time.sleep(1)
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.READY,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)

    @pytest.mark.csp_k8s
    def test_abort_scan(self):
        """
        Test that a subarray is able to process the
        Scan command when its ObsState is READY
        """
        self._configure_scan()
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        LOGGER.info("Issue the Scan command")
        self.midcsp_subarray01.Scan("11")
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.SCANNING,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("Issue the Abort command")
        self.midcsp_subarray01.Abort()
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState', ObsState.ABORTED,
                                           f"Wrong CSP Subarray obsState")
        Poller(10, 0.2).check(prober_obs_state)

    @pytest.mark.csp_k8s
    def test_invoke_configure_command_AFTER_gotoidle(self):
        self._configure_scan()
        self.midcsp_subarray01.GoToIdle()
        LOGGER.info(f"Configuring CSP subarray01")
        configuration_string = prepare_configuration_string()
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f"CSP Subarray not READY")
        Poller(4, 0.2).check(prober_obs_state)


    @pytest.mark.csp_k8s
    def test_obsreset_cbf_AFTER_invalid_configuration(self):
        """
        CSP Subarray sends an invalid json configuration to
        CBF Subarray.
        """
        # setup the test: Subarray DISABLE-IDLE
        self._setup_subarray()
        self._assign_receptors()
        init_state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(init_state, ObsState(obs_state).name))
        # exercise the device
        LOGGER.info(f"Configuring CSP subarray01")
        configuration_string = prepare_configuration_string("test_ConfigureScan_invalid_cbf_json.json")
        self.midcsp_subarray01.Configure(configuration_string)
        # check
        # Subarray final ObsState IDLE
        prober_subarray_flag = Probe(self.midcsp_subarray01, 'failureRaisedFlag', True, f"Failure flag is false")
        Poller(7, 0.2).check(prober_subarray_flag)
        obs_state = self.midcsp_subarray01.obsState
        end_state = self.midcsp_subarray01.State()
        assert obs_state == ObsState.FAULT, f"Current ObsState should be FAULT"
        self.midcsp_subarray01.ObsReset()
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.IDLE, f"CSP Subarray not IDLE")
        Poller(4, 0.2).check(prober_obs_state)

    @pytest.mark.abort
    @pytest.mark.parametrize("elapsed_time", [0.4, 0.41, 0.415, 0.42, 0.43, 0.44, 0.45])
    @pytest.mark.parametrize('execution_number', range(1))
    def test_send_abort_WHILE_in_configuring(self, elapsed_time, execution_number):
        """
        Test that the subarray is able to handle the Abort command when issued 
        at any time.
        The test is repeated several times with different delay times.
        """
        self._setup_subarray()
        self._assign_receptors()
        state = self.midcsp_subarray01.State()
        obs_state = self.midcsp_subarray01.obsState
        LOGGER.info("CSP Subarray State before exercise :{}-{}".format(state, ObsState(obs_state).name))
        # exercise the device
        LOGGER.info(f"Configuring CSP subarray01")
        configuration_string = prepare_configuration_string()
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        time.sleep(elapsed_time)
        self.midcsp_subarray01.Abort()
        # check
        prober_subarray_obstate = Probe(self.midcsp_subarray01, 'obsState', ObsState.ABORTED,
                                        f"Wrong CSP Subarray obsState {self.midcsp_subarray01.obsState}")
        Poller(5, 0.2).check(prober_subarray_obstate)
        obs_state = self.midcsp_subarray01.obsState
        assert obs_state == ObsState.ABORTED

    def test_restart_AFTER_abort_with_subarray_ready(self):
        self._setup_subarray()
        self._assign_receptors()
        # configure the system with invalid json to send it in FAULT
        configuration_string = prepare_configuration_string()
        LOGGER.info(f"Configuring CSP subarray01")
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f"CSP Subarray not READY")
        Poller(4, 0.2).check(prober_obs_state)
        self.midcsp_subarray01.Abort()
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.ABORTED, f"CSP Subarray not ABORTED")
        Poller(4, 0.2).check(prober_obs_state)
        self.midcsp_subarray01.Restart()
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_obs_state)

    def test_restart_AFTER_fault_with_subarray_idle(self):
        """
        CSP Subarray invoke a RESTART after fault with 
        subarray with allocated resources.
        """
        # setup the test: Subarray ON-FAULT
        self._setup_subarray()
        self._assign_receptors()
        # configure the system with invalid json to send it in FAULT
        configuration_string = prepare_configuration_string("test_ConfigureScan_without_configID.json")
        LOGGER.info(f"Configuring CSP subarray01")
        (result_code, msg) = self.midcsp_subarray01.Configure(configuration_string)
        prober_obs_state = Probe(self.midcsp_subarray01, 'obsState',ObsState.FAULT, f"Failure flag is false")
        Poller(7, 0.2).check(prober_obs_state)
        obs_state = self.midcsp_subarray01.obsState
        state = self.midcsp_subarray01.State()
        LOGGER.info("CSPSubarray state: {}-{}".format(state, obs_state))
        # exercise the Subarray invoking Restart
        LOGGER.info(f"Invoke Restart command")
        self.midcsp_subarray01.Restart()
        # check
        # Subarray final ObsState EMPTY
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.EMPTY, f"CSP Subarray not EMPTY")
        Poller(4, 0.2).check(prober_obs_state)

    def test_configureScan_WHEN_subarray_ready(self, midcsp_subarray01, midcsp_master):
        """
        Test that the Configure() command is issued when the Subarray
        state already READY
        """
        self._configure_scan()
        obs_state = midcsp_subarray01.obsState
        assert obs_state == ObsState.READY
        configuration_string = prepare_configuration_string("test_ConfigureScan_basic.json")
        self.midcsp_subarray01.Configure(configuration_string)
        prober_obs_state = Probe(self.midcsp_subarray01, "obsState", ObsState.READY, f"CSP Subarray not READY")
        Poller(4, 0.2).check(prober_obs_state)

'''
    def test_remove_receptors_when_ready(self, midcsp_subarray01):
        """
        Test that the complete deallocation of receptors fails
        when the CspSubarray ObsMode is READY.
        Receptors can be removed only when the subarray
        ObsState is IDLE!
        """
        obs_state = midcsp_subarray01.obsState
        assert obs_state == ObsState.READY
        with pytest.raises(tango.DevFailed) as df:
            midcsp_subarray01.ReleaseAllReceptors()
        if df:
            err_msg = str(df.value.args[0].desc)
            assert "ReleaseAllReceptors command can't be issued when the obsState is READY" in err_msg
        
    def test_remove_receptors_when_idle(self, midcsp_subarray01):
        """
        Test the complete deallocation of receptors from a
        CspSubarray when the subarray is IDLE.
        """
        obs_state = midcsp_subarray01.obsState
        assert obs_state == ObsState.READY
        # command transition to IDLE
        midcsp_subarray01.GoToIdle()
        time.sleep(3)
        obs_state = midcsp_subarray01.obsState
        assert obs_state == ObsState.IDLE
        midcsp_subarray01.ReleaseAllResources()
        time.sleep(3)
        subarray_state = midcsp_subarray01.state()
        assert subarray_state == tango.DevState.OFF
        assert obs_state == ObsState.IDLE

    def test_two_subarrays_configureScan(self, midcsp_subarray01, midcsp_subarray02, midcsp_master, tm1_telstate_proxy):
        """
        Test that the Configure() command is issued when the Subarray
        state is ON and ObsState is IDLE or READY
        """
        midcsp_subarray01.Init()
        time.sleep(5)
        # reinitialize TmTelState simulator to have success
        # in configurin the subarray_01
        tm1_telstate_proxy.Init()
        obs_state1 = midcsp_subarray01.obsState
        obs_state2 = midcsp_subarray02.obsState
        assert ((obs_state1 == 0) and (obs_state2 == 0))
        receptor_membership = midcsp_master.receptorMembership
        receptor_list = midcsp_master.unassignedReceptorIDs
        midcsp_subarray01.AddReceptors([1,4])
        midcsp_subarray02.AddReceptors([2,3])
        time.sleep(2)
        start_time = time.time()
        timeout = False
        while True:
            sub1_state = midcsp_subarray01.state()
            sub2_state = midcsp_subarray02.state()
            if ((sub1_state == tango.DevState.ON) and (sub2_state == tango.DevState.ON)):
                break
            else:
                time.sleep(0.2)
            elapsed_time = time.time() - start_time
            if elapsed_time > 3:
                timeout = True
                break
        assert not timeout
        obs_state1 = midcsp_subarray01.obsState
        obs_state2 = midcsp_subarray02.obsState
        file_path = os.path.dirname(os.path.abspath(__file__))
        f1 = open(file_path + "/configScan_sub1.json")
        f2 = open(file_path + "/configScan_sub2.json")
        midcsp_subarray01.Configure(f1.read().replace("\n", ""))
        time.sleep(2)
        midcsp_subarray02.Configure(f2.read().replace("\n", ""))
        f1.close()
        f2.close()
        start_time = time.time()
        while True:
            obs_state1 = midcsp_subarray01.obsState
            obs_state2 = midcsp_subarray02.obsState
            if ((obs_state1 == 2) and (obs_state2 == 2)):
                break
            else:
                time.sleep(0.2)
            elapsed_time = time.time() - start_time
            # use the default value for the configureDelayExpected
            # need to do some work on setting this value
            if elapsed_time > 10:
                break
        assert ((obs_state2 == 2) and (obs_state1 == 2))
        time.sleep(1)
        assert not midcsp_subarray01.timeoutExpiredFlag 
        midcsp_subarray01.GoToIdle()
        midcsp_subarray02.GoToIdle()
        time.sleep(3)
    '''

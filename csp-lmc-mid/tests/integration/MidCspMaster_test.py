#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# This file is part of the csp-lmc-prototype project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.
"""Contain the tests for the CspMaster."""

# Standard imports
import sys
import os
import time
import logging
import numpy as np
# Tango imports
import tango
from tango import DevState
#from tango.test_context import DeviceTestContext
import pytest

# Path
file_path = os.path.dirname(os.path.abspath(__file__))
# insert base package directory to import global_enum
# module in commons folder
commons_pkg_path = os.path.abspath(os.path.join(file_path, "./csp-lmc-mid"))
sys.path.insert(0, commons_pkg_path)

path = os.path.join(os.path.dirname(__file__), os.pardir)
sys.path.insert(0, os.path.abspath(path))
print(commons_pkg_path)
#Local imports
from ska.base.control_model import AdminMode
from unit.utils import Probe, Poller

LOGGER = logging.getLogger(__name__)
# Device test case

@pytest.mark.usefixtures("midcsp_master", "midcsp_subarray01", "cbf_master")
class TestBase(object):
    fixture_names = ()

    @pytest.fixture(autouse=True)
    def auto_injector_fixture(self, request):
        names = self.fixture_names
        for name in names:
            setattr(self, name, request.getfixturevalue(name))

class TestCspMaster(TestBase):
    fixture_names = ("midcsp_master", "midcsp_subarray01", "cbf_master")

    def _setup_master(self):
        '''
        Set the MID.CSP Master to ON
        '''
        state = self.midcsp_master.State()
        LOGGER.info("master state:{}".format(state))
        if state == DevState.DISABLE:
            self.midcsp_master.adminMode = AdminMode.ONLINE
            prober_admin_mode = Probe(self.midcsp_master, "adminMode", AdminMode.ONLINE, f"CSP Master not ON")
            Poller(3, 0.1).check(prober_admin_mode)
            state = self.midcsp_master.State()
            LOGGER.info("master state:{}".format(state))
        state = self.midcsp_master.State()
        LOGGER.info("master state:{}".format(state))
        if state == DevState.STANDBY:
            argin = ["mid_csp_cbf/sub_elt/master",]
            self.midcsp_master.On(argin)
        state = self.midcsp_master.State()
        if state == DevState.ON:
            return

    def test_On_valid_state(self):
        """
        Test for execution of On command when the CbfMaster is in the right state
        """
        self._setup_master()
        assert self.midcsp_subarray01.state() == DevState.ON

    def test_Standby_valid_state(self):
        """
        Test for execution of On command when the CbfTestMaster is in the right state
        """
        self._setup_master()
        assert self.midcsp_master.state() == DevState.ON
        argin = ["mid_csp_cbf/sub_elt/master",]
        self.midcsp_master.Standby(argin)
        assert self.midcsp_master.state() == DevState.STANDBY

    @pytest.mark.sequence
    @pytest.mark.parametrize('execution_number', range(3))
    def test_sequence_ON_STABDBY_commands(self, execution_number):
        """
        Issue the Standby command just after the On command, without waiting
        on the State value.
        To solve SKB-26 bug, the power commands are now executed in
        synch way.
        Repeat the test a number times = execution_number
        """
        self._setup_master()
        argin = ["mid_csp_cbf/sub_elt/master",]
        self.midcsp_master.Standby(argin)
        assert self.midcsp_subarray01.state() == DevState.OFF
        assert self.midcsp_master.state() == DevState.STANDBY
        self.midcsp_master.On(argin)
        assert self.midcsp_subarray01.state() == DevState.ON
        assert self.midcsp_master.state() == DevState.ON

    '''
    def test_cspmaster_state_WHEN_adminmode_is_offline(self):
        """ Test the adminMode attribute w/r"""
        self._setup_master()
        self.midcsp_master.adminMode = AdminMode.OFFLINE
        prober_admin_mode = Probe(self.midcsp_master, "adminMode", AdminMode.OFFLINE, f"CSP Master not OFFLINE")
        Poller(4, 0.2).check(prober_admin_mode)
        prober_state = Probe(self.midcsp_master, "state", DevState.DISABLE, f"CSP Master not OFFLINE")
        Poller(4, 0.2).check(prober_state)

    def test_cbfmaster_state_WHEN_adminmode_is_offline(self):
        """ Test the adminMode attribute w/r"""
        self._setup_master()
        self.midcsp_master.adminMode = AdminMode.OFFLINE
        prober_admin_mode = Probe(self.midcsp_master, "adminMode", AdminMode.OFFLINE, f"CSP Master not OFFLINE")
        Poller(4, 0.2).check(prober_admin_mode)
        prober_state = Probe(self.midcsp_master, "state", DevState.DISABLE, f"CSP Master not OFFLINE")
        Poller(4, 0.2).check(prober_state)
    def test_properties(self):
        """ Test the device property MaxCapability"""
        self._setup_master()
        capability_list = ['FSP:27', 'VCC:4','SearchBeams:1500', 'TimingBeams:16', 'VlbiBeams:20']
        capability_list.sort()
        #Oss: maxCapability returns a tuple
        assert self.midcsp_master.maxCapabilities == tuple(capability_list)
    '''

    def test_forwarded_attributes(self):
        """ Test the  reportVCCState forwarded attribute"""
        self._setup_master()
        vcc_state = self.midcsp_master.reportVCCState
        vcc_state_cbf = self.cbf_master.reportVCCState
        assert np.array_equal(vcc_state, vcc_state_cbf)

    def test_receptors_unassiged_ids(self):
        """ Test the reading of availableReceptorIDs attribute """
        self._setup_master()
        list_of_receptors = self.midcsp_master.unassignedReceptorIDs
        LOGGER.info("list of receptors:".format(list_of_receptors))
        assert any(list_of_receptors)

    def test_read_capabilities_addresses(self):
        """ Test the reading of availableCapabilities attribute """
        vcc_addresses = self.midcsp_master.vccCapabilitiesAddresses
        assert vcc_addresses
        searchbeams_addresses = self.midcsp_master.searchBeamsAddresses
        assert searchbeams_addresses
        timingbeams_addresses =  self.midcsp_master.timingBeamsAddresses
        assert timingbeams_addresses
        vlbi_addresses = self.midcsp_master.vlbiBeamsAddresses
        assert vlbi_addresses

    def test_available_capabilities(self):
        """ Test the reading of availableCapabilities attribute """
        available_cap = self.midcsp_master.availableCapabilities
        assert available_cap

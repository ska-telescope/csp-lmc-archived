import contextlib
import importlib
import sys
import os
import mock
import pytest
import tango
import time
import json
from mock import Mock, MagicMock

from ska.base.control_model import HealthState, ObsState
from ska.base.commands import  ResultCode
from tango.test_context import DeviceTestContext
from tango import DevState, DevFailed, DeviceProxy
import csp_lmc_common
import csp_lmc_mid
from utils import Probe, Poller
from csp_lmc_common import CspSubarray
from csp_lmc_mid.MidCspSubarrayBase import MidCspSubarrayBase

file_path = os.path.dirname(os.path.abspath(__file__))

def test_midcspsubarray_state_and_obstate_value_AFTER_initialization():
    """
    Test the State and obsState values for the CpSubarray at the
    end of the initialization process.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'State'
    dut_properties = {
            'CspMaster':'mid_csp/elt/master',
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': 'mid_csp_pss/sub_elt/subarray_01',
            'PstSubarray': 'mid_csp_pst/sub_elt/subarray_01',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))

    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        dummy_event = create_dummy_event(cbf_subarray_fqdn, 'state', DevState.OFF)
        event_subscription_map[cbf_subarray_state_attr](dummy_event)
        # Note: the state is in ALARM because the device uses forwarded attributes. 
        #assert tango_context.device.State() == DevState.OFF
        assert tango_context.device.obsState == ObsState.EMPTY

def test_midcspsubarray_state_AFTER_on_command_WITH_exception_raised_by_subelement_subarray():
    """
    Test the behavior of the CspSubarray when one of the sub-element subarray
    raises a DevFailed exception.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, 
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = raise_devfailed_exception 
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        state = tango_context.device.State()
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        # Note: the state is in ALARM because the device uses forwarded attributes. 
        #assert tango_context.device.State() == DevState.FAULT
        assert tango_context.device.obsState == ObsState.EMPTY

def test_midcspsubarray_state_AFTER_on_command_WITH_failed_code_returned_by_subelement_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,

    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_failed
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        #assert tango_context.device.State() == DevState.FAULT
        assert tango_context.device.obsState == ObsState.EMPTY

def test_midcspsubarray_state_AFTER_on_command_WITH_failed_code_returned_by_pss_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_failed
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        #assert tango_context.device.State() == DevState.ON
        assert tango_context.device.obsState == ObsState.EMPTY
        assert tango_context.device.healthState == HealthState.DEGRADED

def test_midcspsubarray_state_after_on_forwarded_to_subelement_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        #assert tango_context.device.State() == DevState.ON
        assert tango_context.device.obsState == ObsState.EMPTY

def test_addreceptors_command_WHEN_subarray_is_in_wrong_state():
    """
    Test the behavior of the MidCspSubarray when receptors are added and the
    subarray is in the wrong state.
    Expected result: exception raised
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
    }
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with pytest.raises(tango.DevFailed) as df:
            receptors_to_add = [1,2,3]
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptors_to_add}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
        if df:
            err_msg = str(df.value.args[0].desc)
            assert "Error executing command AssignResourcesCommand" in err_msg

def test_removereceptors_command_WHEN_subarray_is_in_wrong_state():
    """
    Test the behavior of the MidCspSubarray when receptors are added and the
    subarray is in the wrong state.
    Expected result: exception raised
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.ConfigureCommand.do') as mock_configure_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_configure_do.return_value = (ResultCode.OK, "On command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            tango_context.device.Configure("")
            obs_state = tango_context.device.obsState
            assert obs_state == ObsState.READY
            with pytest.raises(tango.DevFailed) as df:
                tango_context.device.ReleaseAllResources()
            if df:
                err_msg = str(df.value.args[0].desc)
            assert "Error executing command ReleaseAllResourcesCommand" in err_msg

def test_midcspsubarray_obsstate_WHEN_cbfsubarray_raises_an_exception():         
    """
    Test the behavior of the MidCspSubarray when receptors are added to the
    CBF Subarray and it raises an exception during command execution. 
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock,
    }
    cbf_subarray_device_proxy_mock.command_inout_asynch.side_effect = (
        lambda command_name, argument, callback, *args,
        **kwargs: event_subscription_map.update({command_name: callback}))
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with mock.patch('csp_lmc_mid.receptors.Receptors.assigned_to_subarray') as mock_assigned_to_subarray, \
            mock.patch('csp_lmc_mid.receptors.Receptors.unassigned_ids') as mock_unassigned_ids:
            cbf_subarray_device_proxy_mock.On.side_effect = return_ok
            pss_subarray_device_proxy_mock.On.side_effect = return_ok
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate', ObsState.EMPTY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            mock_assigned_to_subarray.return_value = []
            mock_unassigned_ids.return_value = [1,2]
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': [1,2,11,17,21,23]}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            dummy_event = command_callback_with_event_error("AddReceptors", cbf_subarray_fqdn)
            event_subscription_map["AddReceptors"](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)

def test_midcspsubarray_obsstate_AFTER_add_receptors_to_cbf_subarray():         
    """
    Test the behavior of the MidCspSubarray when receptors are added to the
    CBF Subarray. 
    Expected result: state = ON obsState=IDLE
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_ok
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obstate',ObsState.IDLE)
        event_subscription_map[cbf_subarray_state_attr](dummy_event)
        with mock.patch('csp_lmc_mid.receptors.Receptors.list_of_ids') as mock_list_of_ids, \
            mock.patch('csp_lmc_mid.receptors.Receptors.unassigned_ids') as mock_unassigned_ids, \
            mock.patch('csp_lmc_mid.receptors.Receptors.assigned_to_subarray') as mock_assigned_to_subarray,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_list_of_ids.return_value = [1,2,3,4]
            mock_unassigned_ids.return_value = [1,2]
            mock_assigned_to_subarray.return_value = [1,2]
            mock_len.return_value = len([1,2])
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': ["1", "2"]}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.RESOURCING)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.IDLE)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.IDLE, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.IDLE

def test_midcspsubarray_obsstate_AFTER_configure():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    is issued on the CBF Subarray. 
    Expected result: state = ON obsState=READY
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 20
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_ADR22.json")
            tango_context.device.Configure(configuration_string)
            assert tango_context.device.configurationDelayExpected == 20
            #assert tango_context.device.obsState == ObsState.CONFIGURING
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.READY

def test_midcspsubarray_invoke_configure_WHILE_gotoidle_is_running():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    is issued on the CBF Subarray. 
    Expected result: state = ON obsState=READY
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    pss_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.GoToIdle.side_effect = return_ok
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            mock_delay_expected.return_value = 2
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.GoToIdle()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.IDLE)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            assert tango_context.device.obsState == ObsState.CONFIGURING

def test_midcspsubarray_obsstate_AFTER_configure_WITH_wrong_json():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    is issued a wrong configuration file. 
    Expected result: state = ON obsState=IDLE
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_without_configID.json")
            tango_context.device.Configure(configuration_string)
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_AFTER_timeout_during_configuration():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    detect a timeout condition.
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.Configure.side_effect = return_failed
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            #assert tango_context.device.obsState == ObsState.IDLE
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            #assert tango_context.device.obsState == ObsState.CONFIGURING
            prober_obs_state = Probe(tango_context.device, 'timeoutExpiredFlag', True, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_AFTER_cbfsubarray_fault_during_configuration():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    detect a failure condition. 
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        #cbf_subarray_device_proxy_mock.Configure.side_effect = return_failed
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.FAULT)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_flag = Probe(tango_context.device, 'failureRaisedFlag', True, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_flag)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)

def test_midcspsubarray_obsstate_AFTER_abort_request_during_configuration():         
    """
    Test the behavior of the MidCspSubarray when the command Abort
    is invoked during the configuration of the subarray.
    Expected result: state = ON obsState=ABORTED
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.Abort.side_effect = return_ok
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            tango_context.device.Abort()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.ABORTED)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.ABORTED, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.ABORTED

def test_midcspsubarray_obsstate_WHEN_cbfsubarray_is_in_fault_during_abort_request():         
    """
    Test the behavior of the MidCspSubarray when the command Abort
    is invoked and the CBF subarray is in FAULT.
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        #cbf_subarray_device_proxy_mock.Abort.side_effect = return_failed
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            #prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            #Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.Abort()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.FAULT)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_WHEN_abort_invoked_in_resetting():
    """
    Test the behavior of the MidCspSubarray when the command Abort
    is invoked when the subarray is in RESETTING
    Expected result: state = ON obsState=ABORTED
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        #cbf_subarray_device_proxy_mock.Abort.side_effect = return_failed
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.FAULT)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.ObsReset()
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.RESETTING, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.RESETTING)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            tango_context.device.Abort()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.ABORTED)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.ABORTED, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)

def test_midcspsubarray_obsstate_WHEN_restart_invoked_after_cspsubarray_aborted():
    """
    Test the behavior of the MidCspSubarray when the command Restart
    is invoked when the CSPSubarray is in ABORTED state and CBSubarray is READY.
    Expected result: state = ON obsState=EMPTY
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AssignResources OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            param = {
                'subarrayID': 1,
                'dish': {'receptorIDList': receptor_list}}
            json_config = json.dumps(param)
            tango_context.device.AssignResources(json_config)
            configuration_string = load_json_file("test_ConfigureScan_without_configID.json")
            tango_context.device.Configure(configuration_string)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.Restart()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.EMPTY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.EMPTY, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)

def test_midcspsubarray_state_AFTER_on_command_WITH_failed_code_returned_by_pss_subarray():
    """
    Test the behavior of the CspSubarray when on the sub-element subarray
    return a FAILED code.
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
    }
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: pss_subarray_device_proxy_mock
    }

    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.On.side_effect = return_ok
        pss_subarray_device_proxy_mock.On.side_effect = return_failed
        prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
        Poller(3, 0.1).check(prober_state)
        tango_context.device.On()
        #assert tango_context.device.State() == DevState.ON
        assert tango_context.device.obsState == ObsState.EMPTY
        assert tango_context.device.healthState == HealthState.DEGRADED

def return_ok():
    """
    Return a FAILED code in the execution of a device method.
    """
    message = "CBF Subarray Oncommand OK"
    return (ResultCode.OK, message)

def return_started():
    """
    Return a FAILED code in the execution of a device method.
    """
    message = "CBF Subarray Oncommand OK"
    return (ResultCode.STARTED, message)

def return_failed():
    """
    Return a FAILED code in the execution of a device method.
    """
    print("return failed")
    return (ResultCode.FAILED, "On Command failed")

def raise_devfailed_exception():
    """
    Raise an exception to test the failure of a device command
    """
    print("raise_devfailed_exception")
    tango.Except.throw_exception("Commandfailed", "This is error message for devfailed",
                                 " ", tango.ErrSeverity.ERR)
def mock_event_dev_name(device_name):
    return device_name

def create_dummy_event(cbf_subarray_fqdn, attr_name, event_value):
    """
    Create a mocked event object to test the event callback method
    associate to the attribute at subscription.
    param: cbf_subarray_fqdn the CBF Subarray FQDN
           event_value the expected value
    return: the fake event
    """
    fake_event = Mock()
    fake_event.err = False
    fake_event.attr_name = f"{cbf_subarray_fqdn}/{attr_name}"
    fake_event.attr_value.value = event_value
    fake_event.attr_value.name = attr_name
    fake_event.device.name = cbf_subarray_fqdn
    fake_event.device.dev_name.side_effect=(lambda *args, **kwargs: mock_event_dev_name(cbf_subarray_fqdn))
    return fake_event

def command_callback_with_event_error(command_name, cbf_subarray_fqdn):
    fake_event = MagicMock()
    fake_event.err = False
    fake_event.cmd_name = f"{command_name}"
    fake_event.device.dev_name.side_effect=(lambda *args, **kwargs: mock_event_dev_name(cbf_subarray_fqdn))
    fake_event.argout = [ResultCode.FAILED, "Command Failed"]
    return fake_event

def raise_exception(command_name):
    return raise_devfailed_exception()

def load_json_file(file_name):
    print(file_path)
    print(os.path.dirname(__file__))
    path= os.path.join(os.path.dirname(__file__), '../test_data' , file_name)
    with open(path, 'r') as f:
        configuration_string = f.read().replace("\n", "")
        return configuration_string

@contextlib.contextmanager
def fake_tango_system(device_under_test, initial_dut_properties={}, proxies_to_mock={},
        device_proxy_import_path='tango.DeviceProxy'):

    with mock.patch(device_proxy_import_path) as patched_constructor:
        patched_constructor.side_effect = lambda device_fqdn: proxies_to_mock.get(device_fqdn, Mock())
        patched_module = importlib.reload(sys.modules[device_under_test.__module__])

    device_under_test = getattr(patched_module, device_under_test.__name__)

    device_test_context = DeviceTestContext(device_under_test, properties=initial_dut_properties)
    device_test_context.start()
    yield device_test_context
    device_test_context.stop()

'''
def test_midcspsubarray_obsstate_WHEN_cbfsubarray_abort_returns_failed():         
    """
    Test the behavior of the MidCspSubarray when the command Abort
    is invoked and the CBF subarray is in FAULT.
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        cbf_subarray_device_proxy_mock.Abort.side_effect = return_failed
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AssignResourcesCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AddReceptors OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            tango_context.device.On()
            tango_context.device.AddReceptors(receptor_list)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            tango_context.device.Abort()
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_WHEN_timeout_in_abort_request_during_configuring():
    """
    Test the behavior of the MidCspSubarray when the command Abort
    is invoked during the configuration of the subarray.
    Expected result: state = ON obsState=ABORTED
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties,
                           proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AddReceptorsCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_common.CspSubarray._get_expected_delay') as mock_delay_expected,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AddReceptors OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            mock_delay_expected.return_value = 2
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            tango_context.device.AddReceptors(receptor_list)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            #prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            #Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.Abort()
            prober_obs_state = Probe(tango_context.device, 'timeoutExpiredFlag', True, f"Wrong CspSubarray state")
            Poller(10, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_AFTER_configure_WITH_cbf_returning_FAULT():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    detect a failure condition. 
    Expected result: state = ON obsState=FAULT
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.command_inout_asynch.side_effect = (
        lambda command_name, argument, callback, *args,
        **kwargs: event_subscription_map.update({command_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AddReceptorsCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AddReceptors OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            tango_context.device.AddReceptors(receptor_list)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = command_callback_with_event_error("ConfigureScan", cbf_subarray_fqdn)
            event_subscription_map["ConfigureScan"](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'failureRaisedFlag', True, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.FAULT

def test_midcspsubarray_obsstate_AFTER_end_scan():         
    """
    Test the behavior of the MidCspSubarray when the command Configure
    is issued on the CBF Subarray. 
    Expected result: state = ON obsState=READY
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    cbf_subarray_state_attr = 'obsState'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    cbf_subarray_device_proxy_mock.Scan.side_effect = return_started
    #cbf_subarray_device_proxy_mock.Scan.side_effect = return_ok
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AddReceptorsCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AddReceptors OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            #mock_configure_do.return_value = (ResultCode.OK, "Configure command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            tango_context.device.AddReceptors(receptor_list)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.Scan("1")
            time.sleep(1)
            tango_context.device.EndScan()
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.READY)
            event_subscription_map[cbf_subarray_state_attr](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.READY, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            assert tango_context.device.obsState == ObsState.READY

def test_midcspsubarray_obsreset():         
    """
    Test the behavior of the MidCspSubarray when the command ObsReset
    is issued.
    Expected result: state = ON obsState=IDLE
    """
    device_under_test = MidCspSubarrayBase
    cbf_subarray_fqdn = 'mid_csp_cbf/sub_elt/subarray_01'
    pss_subarray_fqdn = 'mid_csp_pss/sub_elt/subarray_01'
    dut_properties = {
            'CbfSubarray': cbf_subarray_fqdn,
            'PssSubarray': pss_subarray_fqdn,
            'CspMaster':'mid_csp/elt/master',
            'SubID': '1',
    }
    event_subscription_map = {}
    cbf_subarray_device_proxy_mock = Mock()
    pss_subarray_device_proxy_mock = Mock()
    cbf_subarray_device_proxy_mock.subscribe_event.side_effect = (
       lambda attr_name, event_type, callback, *args, **kwargs: event_subscription_map.update({attr_name: callback}))
    proxies_to_mock = {
        cbf_subarray_fqdn: cbf_subarray_device_proxy_mock,
        pss_subarray_fqdn: cbf_subarray_device_proxy_mock,
    }
    receptor_list = [1,2,3]
    with fake_tango_system(device_under_test, initial_dut_properties=dut_properties, proxies_to_mock=proxies_to_mock) as tango_context:
        with  mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.AddReceptorsCommand.do') as mock_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.OnCommand.do') as mock_on_do,\
            mock.patch('csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase.__len__') as mock_len:
            mock_len.return_value = len(receptor_list)
            mock_do.return_value = (ResultCode.OK, "AddReceptors OK")
            mock_on_do.return_value = (ResultCode.OK, "On command OK")
            prober_state = Probe(tango_context.device, 'State', DevState.OFF, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_state)
            tango_context.device.On()
            tango_context.device.AddReceptors(receptor_list)
            configuration_string = load_json_file("test_ConfigureScan_ADR4.json")
            tango_context.device.Configure(configuration_string)
            dummy_event = create_dummy_event(cbf_subarray_fqdn, 'obsstate',ObsState.FAULT)
            event_subscription_map['obsState'](dummy_event)
            prober_obs_state = Probe(tango_context.device, 'obsState', ObsState.FAULT, f"Wrong CspSubarray state")
            Poller(3, 0.1).check(prober_obs_state)
            tango_context.device.ObsReset()
            assert tango_context.device.obsState == ObsState.RESETTING
'''

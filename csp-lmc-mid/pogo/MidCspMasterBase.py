# -*- coding: utf-8 -*-
#
# This file is part of the MidCspMasterBase project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MidCspMasterBase class

The base class for MID CspMAster.
Fuctionality to monitor CSP.LMC Capabilities are 
implemented in separate TANGO Devices.
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
import enum
from CspMaster import CspMaster
# Additional import
# PROTECTED REGION ID(MidCspMasterBase.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  MidCspMasterBase.additionnal_import

__all__ = ["MidCspMasterBase", "main"]


class HealthState(enum.IntEnum):
    """Python enumerated type for HealthState attribute."""


class AdminMode(enum.IntEnum):
    """Python enumerated type for AdminMode attribute."""


class ControlMode(enum.IntEnum):
    """Python enumerated type for ControlMode attribute."""


class SimulationMode(enum.IntEnum):
    """Python enumerated type for SimulationMode attribute."""


class TestMode(enum.IntEnum):
    """Python enumerated type for TestMode attribute."""


class CspCbfHealthState(enum.IntEnum):
    """Python enumerated type for CspCbfHealthState attribute."""


class CspPssHealthState(enum.IntEnum):
    """Python enumerated type for CspPssHealthState attribute."""


class CspPstHealthState(enum.IntEnum):
    """Python enumerated type for CspPstHealthState attribute."""


class CspCbfAdminMode(enum.IntEnum):
    """Python enumerated type for CspCbfAdminMode attribute."""


class CspPssAdminMode(enum.IntEnum):
    """Python enumerated type for CspPssAdminMode attribute."""


class CspPstAdminMode(enum.IntEnum):
    """Python enumerated type for CspPstAdminMode attribute."""


class LoggingLevel(enum.IntEnum):
    """Python enumerated type for LoggingLevel attribute."""


class MidCspMasterBase(CspMaster):
    """
    The base class for MID CspMAster.
    Fuctionality to monitor CSP.LMC Capabilities are 
    implemented in separate TANGO Devices.

    **Properties:**

    - Device Property
        VCCsMonitor
            - TANGO Device to monitor the Mid.CSP VCCs Capabilities\ndevices.
            - Type:'DevString'
        FSPsMonitor
            - TANGO Device to monitor the Mid.CSP FSPs Capabilities\ndevices.
            - Type:'DevString'
    """
    __metaclass__ = DeviceMeta
    # PROTECTED REGION ID(MidCspMasterBase.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspMasterBase.class_variable

    # -----------------
    # Device Properties
    # -----------------


















    VCCsMonitor = device_property(
        dtype='DevString', default_value="mid_csp/capability_monitor/receptors"
    )

    FSPsMonitor = device_property(
        dtype='DevString', default_value="mid_csp/capability_monitor/fsps"
    )

    # ----------
    # Attributes
    # ----------

















































    availableCapabilities = attribute(
        dtype=('DevString',),
        max_dim_x=20,
        doc="A list of available number of instances of each capability type, e.g. `CORRELATOR:512`, `PSS-BEAMS:4`.",
    )



    receptorMembership = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="Receptor Memebership",
        doc="The receptors affiliation to MID CSP sub-arrays.",
    )

    unassignedReceptorIDs = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="Unassigned receptors IDs",
        doc="The list of available receptors IDs.",
    )

    timingBeamsAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=16,
        label="Timing Beams Capability Addresses",
        doc="The list of Timing Beam Capabilities FQDNs",
    )


    vccCapabilitiesAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=197,
        label="VCC Capabilities Addresses",
        doc="The list of VCC Capabilities FQDNs",
    )

    fspCapabilitiesAddresses = attribute(
        dtype=('DevString',),
        max_dim_x=27,
        label="FSP Capabilities Addresses",
        doc="The list of FSP Capabilites FQDNs",
    )

    reportSearchBeamState = attribute(
        name="reportSearchBeamState",
        label="SearchBeam Capabilities State",
        forwarded=True
    )
    reportSearchBeamHealthState = attribute(
        name="reportSearchBeamHealthState",
        label="SearchBeam Capabilities healthState",
        forwarded=True
    )
    reportSearchBeamObsState = attribute(
        name="reportSearchBeamObsState",
        label="SearchBeam Capabilities obsState",
        forwarded=True
    )
    reportSearchBeamAdminMode = attribute(
        name="reportSearchBeamAdminMode",
        label="SearchBeam Capabilities adminMode",
        forwarded=True
    )
    reportTimingBeamState = attribute(
        name="reportTimingBeamState",
        label="TimingBeam Capabilities State",
        forwarded=True
    )
    reportTimingBeamHealthState = attribute(
        name="reportTimingBeamHealthState",
        label="TimingBeam Capabilities healthState",
        forwarded=True
    )
    reportTimingBeamObsState = attribute(
        name="reportTimingBeamObsState",
        label="TimingBeam Capabilities obsState",
        forwarded=True
    )
    reportTimingBeamAdminMode = attribute(
        name="reportTimingBeamAdminMode",
        label="TimingBeam Capabilities adminMode",
        forwarded=True
    )
    reportVlbiBeamState = attribute(
        name="reportVlbiBeamState",
        label="VlbiBeam Capabilities State",
        forwarded=True
    )
    reportVlbiBeamHealthState = attribute(
        name="reportVlbiBeamHealthState",
        label="VlbiBeam Capabilities healthState",
        forwarded=True
    )
    reportVlbiBeamObsState = attribute(
        name="reportVlbiBeamObsState",
        label="VlbiBeam Capabilities obsState",
        forwarded=True
    )
    reportVlbiBeamAdminMode = attribute(
        name="reportVlbiBeamAdminMode",
        label="VlbiBeam Capabilities adminMode",
        forwarded=True
    )
    reservedSearchBeamIDs = attribute(
        name="reservedSearchBeamIDs",
        label="IDs of reserved SeachBeam Capabilities",
        forwarded=True
    )
    unassignedVlbiBeamIDs = attribute(
        name="unassignedVlbiBeamIDs",
        label="Unassigned VlbiBeam Capabilities IDs",
        forwarded=True
    )
    unassignedTimingBeamIDs = attribute(
        name="unassignedTimingBeamIDs",
        label="Unassigned TimingBeam Capabilities IDs",
        forwarded=True
    )
    unassignedSearchBeamIDs = attribute(
        name="unassignedSearchBeamIDs",
        label="Unassigned SeachBeam Capabilities IDs",
        forwarded=True
    )
    reservedSearchBeamNum = attribute(
        name="reservedSearchBeamNum",
        label="Number of reserved SeachBeam Capabilities",
        forwarded=True
    )
    searchBeamMembership = attribute(
        name="searchBeamMembership",
        label="SearchBeam Membership",
        forwarded=True
    )
    timingBeamMembership = attribute(
        name="timingBeamMembership",
        label="TimingBeam Membership",
        forwarded=True
    )
    vlbiBeamMembership = attribute(
        name="vlbiBeamMembership",
        label="VlbiBeam Membership",
        forwarded=True
    )
    reportVCCState = attribute(
        name="reportVCCState",
        label="reportVCCState",
        forwarded=True
    )
    reportVCCHealthState = attribute(
        name="reportVCCHealthState",
        label="reportVCCHealthState",
        forwarded=True
    )
    reportVCCAdminMode = attribute(
        name="reportVCCAdminMode",
        label="reportVCCAdminMode",
        forwarded=True
    )
    reportFSPState = attribute(
        name="reportFSPState",
        label="reportFSPState",
        forwarded=True
    )
    reportFSPHealthState = attribute(
        name="reportFSPHealthState",
        label="reportFSPHealthState",
        forwarded=True
    )
    reportFSPAdminMode = attribute(
        name="reportFSPAdminMode",
        label="reportFSPAdminMode",
        forwarded=True
    )
    fspMembership = attribute(
        name="fspMembership",
        label="fspMembership",
        forwarded=True
    )
    vccMembership = attribute(
        name="vccMembership",
        label="vccMembership",
        forwarded=True
    )
    numOfUnassignedVlbiBeams = attribute(
        name="numOfUnassignedVlbiBeams",
        label="Num of onassigned VlbiBeam Capabilities IDs",
        forwarded=True
    )
    numOfUnassignedTimingBeams = attribute(
        name="numOfUnassignedTimingBeams",
        label="Num of unassigned TimingBeam Capabilities IDs",
        forwarded=True
    )
    numOfUnassignedSearchBeams = attribute(
        name="numOfUnassignedSearchBeams",
        label="Num of unassigned SeachBeam Capabilities IDs",
        forwarded=True
    )
    numOfReservedSearchBeams = attribute(
        name="numOfReservedSearchBeams",
        label="Number of reserved SeachBeam Capabilities",
        forwarded=True
    )
    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the MidCspMasterBase."""
        CspMaster.init_device(self)
        # PROTECTED REGION ID(MidCspMasterBase.init_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspMasterBase.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspMasterBase.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspMasterBase.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspMasterBase.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspMasterBase.delete_device
    # ------------------
    # Attributes methods
    # ------------------

    def read_availableCapabilities(self):
        # PROTECTED REGION ID(MidCspMasterBase.availableCapabilities_read) ENABLED START #
        """Return the availableCapabilities attribute."""
        return ('',)
        # PROTECTED REGION END #    //  MidCspMasterBase.availableCapabilities_read

    def read_receptorMembership(self):
        # PROTECTED REGION ID(MidCspMasterBase.receptorMembership_read) ENABLED START #
        """Return the receptorMembership attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspMasterBase.receptorMembership_read

    def read_unassignedReceptorIDs(self):
        # PROTECTED REGION ID(MidCspMasterBase.unassignedReceptorIDs_read) ENABLED START #
        """Return the unassignedReceptorIDs attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspMasterBase.unassignedReceptorIDs_read

    def read_timingBeamsAddresses(self):
        # PROTECTED REGION ID(MidCspMasterBase.timingBeamsAddresses_read) ENABLED START #
        """Return the timingBeamsAddresses attribute."""
        return ('',)
        # PROTECTED REGION END #    //  MidCspMasterBase.timingBeamsAddresses_read

    def read_vccCapabilitiesAddresses(self):
        # PROTECTED REGION ID(MidCspMasterBase.vccCapabilitiesAddresses_read) ENABLED START #
        """Return the vccCapabilitiesAddresses attribute."""
        return ('',)
        # PROTECTED REGION END #    //  MidCspMasterBase.vccCapabilitiesAddresses_read

    def read_fspCapabilitiesAddresses(self):
        # PROTECTED REGION ID(MidCspMasterBase.fspCapabilitiesAddresses_read) ENABLED START #
        """Return the fspCapabilitiesAddresses attribute."""
        return ('',)
        # PROTECTED REGION END #    //  MidCspMasterBase.fspCapabilitiesAddresses_read

    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    """Main function of the MidCspMasterBase module."""
    # PROTECTED REGION ID(MidCspMasterBase.main) ENABLED START #
    return run((MidCspMasterBase,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspMasterBase.main


if __name__ == '__main__':
    main()

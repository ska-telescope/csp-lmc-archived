"""
A module defining a list of fixture functions that are shared across all the skabase
tests.
"""
#from __future__ import absolute_import
#import mock
import pytest

#sys.path.insert(0, "../commons")

import tango
#from tango.test_context import DeviceTestContext

#import global_enum

@pytest.fixture(scope="class")
def midcsp_master():
    """Create DeviceProxy for the MidCspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('MidCspMasterBase')
    for instance in instance_list.value_string:
        try:
            return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def midcsp_subarray01():
    """Create DeviceProxy for the CspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('MidCspSubarrayBase')
    for instance in instance_list.value_string:
        try:
            if "subarray_01" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def midcsp_subarray02():
    """Create DeviceProxy for the CspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('MidCspSubarrayBase')
    for instance in instance_list.value_string:
        try:
            if "subarray_02" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def cbf_subarray01():
    """Create DeviceProxy for the CspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('CbfSubarray')
    for instance in instance_list.value_string:
        try:
            if "subarray_01" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def cbf_subarray02():
    """Create DeviceProxy for the CspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('CbfSubarray')
    for instance in instance_list.value_string:
        try:
            if "subarray_02" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def cbf_master():
    """Create DeviceProxy for the CspMaster device
       to test the device with the TANGO DB
    """
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('CbfMaster')
    for instance in instance_list.value_string:
        try:
            return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

@pytest.fixture(scope="class")
def tm1_telstate_proxy():
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('TmCspSubarrayLeafNodeTest')
    for instance in instance_list.value_string:
        try:
            if "subarray_01" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue


@pytest.fixture(scope="class")
def tm2_telstate_proxy():
    database = tango.Database()
    instance_list = database.get_device_exported_for_class('TmCspSubarrayLeafNodeTest')
    for instance in instance_list.value_string:
        try:
            if "subarray_02" in instance:
                return tango.DeviceProxy(instance)
        except tango.DevFailed:
            continue

from tango.server import run

from csp_lmc_common.CspCapabilityMonitor import CspCapabilityMonitor
from csp_lmc_common.CspSearchBeamsMonitor import CspSearchBeamsMonitor

def main(args=None, **kwargs):
    return run(classes=(CspCapabilityMonitor, CspSearchBeamsMonitor), args=args, **kwargs)


if __name__ == '__main__':
    main()

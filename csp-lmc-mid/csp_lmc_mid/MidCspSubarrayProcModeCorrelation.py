# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarrayProcModeCorrelation project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MID CSP Subarray Processing Mode Correlation Capability

The class exposes parameters nd commands required for monitor and
control of the correlator functions for a single MID Csp Subarray.
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from csp_lmc_common.CspSubarrayProcModeCorrelation import CspSubarrayProcModeCorrelation
# Additional import
# PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.additionnal_import

__all__ = ["MidCspSubarrayProcModeCorrelation", "main"]


class MidCspSubarrayProcModeCorrelation(CspSubarrayProcModeCorrelation):
    """
    The class exposes parameters nd commands required for monitor and
    control of the correlator functions for a single MID Csp Subarray.

    **Properties:**

    - Device Property
    
    
    """
    # PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------

















    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the MidCspSubarrayProcModeCorrelation."""
        CspSubarrayProcModeCorrelation.init_device(self)
        # PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.init_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(MidCspSubarrayProcModeCorrelation.main) ENABLED START #
    return run((MidCspSubarrayProcModeCorrelation,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspSubarrayProcModeCorrelation.main

if __name__ == '__main__':
    main()

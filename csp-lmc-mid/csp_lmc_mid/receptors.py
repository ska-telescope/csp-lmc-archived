import tango
import logging
import time
from tango import DeviceProxy

module_logger = logging.getLogger(__name__)
class Receptors:
    """
    Class to handle information about the Mid receptors.
    """
    def __init__(self, device, logger=None):
        self.device = device
        self._csp_master_fqdn = device.CspMaster
        self.logger = logger or module_logger

    def cbf_master_address(self):
        """
        Return the address of the MID CspMaster holding the
        information about receptors.
        """
        try:
            proxy = self.connect(self._csp_master_fqdn)
            return proxy.cbfMasterAddress
        except tango.DevFailed as tango_err:
            tango.Except.re_throw_exception(tango_err,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)
    def connect(self, fqdn):
        """
        Establish the connection with the specified
        TANGO device
        """
        try: 
            proxy = DeviceProxy(fqdn)
            proxy.ping()
            return proxy
        except tango.DevFailed as tango_ex:
            module_logger.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)
    def receptor_vcc_map(self):
        """
        Return the dictionary with the map of VCC ids and
        receptors ids.
        """
        receptor_to_vcc_map= {}
        try:
            cbf_master_addr = self.cbf_master_address()
            proxy = self.connect(cbf_master_addr)
            receptor_to_vcc = proxy.receptorToVcc
            receptor_to_vcc_map = dict([int(ID) for ID in pair.split(":")]
                                       for pair in receptor_to_vcc)
            return receptor_to_vcc_map
        except tango.DevFailed as tango_ex:
            module_logger.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)
    def list_of_ids(self):
        """
        Return the list of the receptor ids deployed by the
        system.
        """
        try:
            value = self.receptor_vcc_map()
            return list(value.keys())
        except tango.DevFailed as tango_ex:
            self.logging.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)

    def unassigned_ids(self):
        """
        Return the list of the receptor ids not assigned to any
        subarray.
        """
        value = []
        try:
            proxy = self.connect(self._csp_master_fqdn)
            value = proxy.unassignedReceptorIDs
            return value
        except tango.DevFailed as tango_ex:
            module_logger.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)

    def subarray_affiliation(self):
        """
        Return the list with the subarray affilition for each
        receptor.
        """
        value = []
        try:
            proxy = self.connect(self._csp_master_fqdn)
            value = proxy.receptorMembership
            module_logger.debug("receptorMembership:{}".format(value))
            return value
        except tango.DevFailed as tango_ex:
            module_logger.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)

    def assigned_to_subarray(self, _sub_id):
        """
        Return the list of the assigned receptors to the subarray.
        """
        assigned_receptors = []
        sub_id = int(_sub_id)
        try:
            proxy = self.connect(self.device.CbfSubarray)
            #receptor_membership = self.subarray_affiliation()
            #assigned_receptors = [receptor_id + 1 for receptor_id, e in enumerate(receptor_membership) if e == int(sub_id)]
            assigned_receptors = proxy.receptors
            module_logger.debug(assigned_receptors)
            return assigned_receptors
        except tango.DevFailed as tango_ex:
            module_logger.error(tango_ex.args[0].desc)
            tango.Except.re_throw_exception(tango_ex,
                                            "Connection failed",
                                            "Receptors class",
                                             tango.ErrSeverity.ERR)


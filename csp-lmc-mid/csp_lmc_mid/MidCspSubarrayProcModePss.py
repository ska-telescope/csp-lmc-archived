# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarrayProcModePss project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MID CSP Subarray Processing Mode PSS Capability

The class exposes parameters and commands required for monitor and
control of the Processing PSS Mode for a single MID Csp Subarray.
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from csp_lmc_common.CspSubarrayProcModePss import CspSubarrayProcModePss
# Additional import
# PROTECTED REGION ID(MidCspSubarrayProcModePss.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  MidCspSubarrayProcModePss.additionnal_import

__all__ = ["MidCspSubarrayProcModePss", "main"]


class MidCspSubarrayProcModePss(CspSubarrayProcModePss):
    """
    The class exposes parameters and commands required for monitor and
    control of the Processing PSS Mode for a single MID Csp Subarray.

    **Properties:**

    - Device Property
    """
    # PROTECTED REGION ID(MidCspSubarrayProcModePss.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspSubarrayProcModePss.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------

















    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the MidCspSubarrayProcModePss."""
        CspSubarrayProcModePss.init_device(self)
        # PROTECTED REGION ID(MidCspSubarrayProcModePss.init_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePss.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspSubarrayProcModePss.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePss.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspSubarrayProcModePss.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePss.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(MidCspSubarrayProcModePss.main) ENABLED START #
    return run((MidCspSubarrayProcModePss,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspSubarrayProcModePss.main

if __name__ == '__main__':
    main()

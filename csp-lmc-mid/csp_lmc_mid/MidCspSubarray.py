from tango.server import run

from MidCspSubarrayBase import MidCspSubarrayBase
from MidCspSubarrayProcModeCorrelation import MidCspSubarrayProcModeCorrelation
from MidCspSubarrayProcModePss import MidCspSubarrayProcModePss
from MidCspSubarrayProcModePst import MidCspSubarrayProcModePst
from MidCspSubarrayProcModeVlbi import MidCspSubarrayProcModeVlbi

def main(args=None, **kwargs):
    return run(classes=(MidCspSubarrayProcModeCorrelation,
               MidCspSubarrayProcModePss, MidCspSubarrayProcModePst,
               MidCspSubarrayProcModeVlbi,MidCspSubarrayBase), args=args, **kwargs)


if __name__ == '__main__':
    main()

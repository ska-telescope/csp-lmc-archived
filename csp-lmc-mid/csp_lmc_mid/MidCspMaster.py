from tango.server import run
from MidCspMasterBase import MidCspMasterBase

def main(args=None, **kwargs):
    return run(classes=(MidCspMasterBase,), args=args, **kwargs)


if __name__ == '__main__':
    main()

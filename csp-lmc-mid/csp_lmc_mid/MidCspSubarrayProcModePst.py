# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarrayProcModePst project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MID CSP Subarray Processing Mode PST Capability

The class exposes parameters and commands required for monitor and
control the Processing Mode PST for a single MID Csp Subarray.
"""

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from csp_lmc_common.CspSubarrayProcModePst import CspSubarrayProcModePst
# Additional import
# PROTECTED REGION ID(MidCspSubarrayProcModePst.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  MidCspSubarrayProcModePst.additionnal_import

__all__ = ["MidCspSubarrayProcModePst", "main"]


class MidCspSubarrayProcModePst(CspSubarrayProcModePst):
    """
    The class exposes parameters and commands required for monitor and
    control the Processing Mode PST for a single MID Csp Subarray.

    **Properties:**

    - Device Property
    
    """
    # PROTECTED REGION ID(MidCspSubarrayProcModePst.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspSubarrayProcModePst.class_variable

    # -----------------
    # Device Properties
    # -----------------

    # ----------
    # Attributes
    # ----------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the MidCspSubarrayProcModePst."""
        CspSubarrayProcModePst.init_device(self)
        # PROTECTED REGION ID(MidCspSubarrayProcModePst.init_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePst.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspSubarrayProcModePst.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePst.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspSubarrayProcModePst.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModePst.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(MidCspSubarrayProcModePst.main) ENABLED START #
    return run((MidCspSubarrayProcModePst,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspSubarrayProcModePst.main

if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarrayBase project
#
# INAF, SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MidCspSubarrayBase

The base class for MID CspSubarray.
Fuctionality to monitor assigned CSP.LMC Capabilities,
as well as inherent Capabilities, are implemented in 
separate TANGO Devices.
"""
# PROTECTED REGION ID (MidCspSubarrayBase.standardlibray_import) ENABLED START #
# Python standard library
import sys
import os

import threading
import time
import json
from collections import defaultdict
import logging
import numpy

# PROTECTED REGION END# //MidCspSubarrayBase.standardlibray_import

# tango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device, DeviceMeta
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, EventType, DevState
from tango import AttrWriteType, DeviceProxy
# Additional import
# PROTECTED REGION ID(MidCspSubarray.additional_import) ENABLED START #
from ska.base import SKASubarray, SKASubarrayStateModel
from ska.base.commands import ActionCommand, ResponseCommand, ResultCode
from ska.base.faults import CapabilityValidationError
from ska.base.control_model import HealthState, AdminMode, ObsState
# import CSP.LMC Common package
from csp_lmc_common.utils.cspcommons import CmdExecState
#from csp_lmc_common.CspSubarray import CspSubarray, CspSubarrayStateModel
from csp_lmc_common.CspSubarray import CspSubarray
from csp_lmc_mid.receptors import Receptors
from csp_lmc_common.csp_manage_json import JsonConfiguration
# PROTECTED REGION END #    //  MidCspSubarrayBase.additionnal_import

__all__ = ["MidCspSubarrayBase", "main"]

class MidCspSubarrayBase(CspSubarray):
    """
    The base class for MID CspSubarray.
    Fuctionality to monitor assigned CSP.LMC Capabilities,
    as well as inherent Capabilities, are implemented in 
    separate TANGO Devices.

    **Properties:**

    - Class Property

    - Device Property
    """

    # PROTECTED REGION ID(MidCspSubarrayBase.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspSubarrayBase.class_variable
    # PROTECTED REGION ID(MidCspSubarrayBase.private_methods) ENABLED START #
    class InitCommand(CspSubarray.InitCommand):
        """
        A class for the SKASubarray's init_device() "command".
        """
        def do(self):
            """
            Stateless hook for device initialisation.

            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            (result_code, message) = super().do()
            device = self.target
            device._assigned_vcc = []
            device._assigned_fsp = []
            #device._receptor_to_vcc_map = {}
            #device._receptor_id_list = []
            device._receptors = Receptors(device, self.logger)
            message = "MidCspSubarray Init command completed OK"
            self.logger.info(message)
            return (result_code, message)

    class OnCommand(CspSubarray.OnCommand):
        # NOTE: To remove when CBF Subarray implements the correct state
        def do(self):
            return super().do()
            #return (ResultCode.OK, "On command completed OK")

    class AssignResourcesCommand(CspSubarray.AssignResourcesCommand):
        #TODO: aggiungere logica riconoscimento sottoelementi
        def do(self, argin):
            """
            Stateless hook for AssignResources() command functionality.
            :param argin: The resources to be assigned.
            :type argin: string (JSON formatted)
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            :raise: ValueError exception.
            """
            super().do(argin)
            try:
                resources_dict = json.loads(argin)
                subarray_id = resources_dict['subarrayID']
                if subarray_id != int(self.target.SubID):
                    msg = f"Mismatch in subarrayID. Received: {subarray_id} {self.target.SubID}"
                    self.logger.error(msg)
                    return (ResultCode.FAILED, msg)
                receptor_list = list(map(int, resources_dict['dish']['receptorIDList']))
            except (KeyError,  json.JSONDecodeError) as error:
                msg = f"Something wrong in Json input string: {error}"
                return (ResultCode.FAILED, msg)
            return self._add_receptors(receptor_list)

        def _add_receptors(self, argin):
            """
            Issue the command AddReceptors to the Mid.CBF Subarray.

            :param argin: The list of receptors ID to assign to the subarray
            :type argin: list of integer
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            device = self.target
            # check if the CbfSubarray TANGO device is already running
            # and the proxy registered
            if not device._is_sc_subarray_running(device.CbfSubarray):
                return (ResultCode.FAILED, "AssignResources command FAILED")
            proxy = device._sc_subarray_proxies[device.CbfSubarray]
            device._reconfiguring = False
            if self.state_model._obs_state == ObsState.IDLE: 
                device._reconfiguring = True
            # forward the command to the CbfSubarray
            try: 
                receptor_to_assign = self._validate_receptors_list(argin)
                if not any(receptor_to_assign):
                    return (ResultCode.OK, "AssignResources end OK")
                device._timeout_expired = False
                device._failure_raised = False
                proxy.command_inout_asynch("AddReceptors", receptor_to_assign, device._cmd_ended_cb)
                
                device._command_thread['addreceptors'] = threading.Thread(target=device._monitor_add_receptors,
                                                                        name="Thread-AddReceptors",
                                                                        args=(receptor_to_assign,))
                device._sc_subarray_cmd_starting_time[device.CbfSubarray] = time.time()
                if device._cmd_execution_state['addreceptors'] != CmdExecState.FAILED:
                    device._cmd_execution_state['addreceptors'] = CmdExecState.RUNNING
                device._command_thread['addreceptors'].start()
                return (ResultCode.STARTED, "AssignResourcess Started")
            except tango.DevFailed as tango_err:
                return (ResultCode.FAILED, "Failure in AssignResources")

        def _validate_receptors_list(self, argin):
            """
            Validate the list of the receptor IDs.
            The method check if:
            - the receptor Ids are valid that is are within the [1, 198] range
            - the receptor Ids are not already allocated to other subarray
            """

            # PROTECTED REGION ID(CspSubarray.AddReceptors) ENABLED START #
            # Each vcc_id map to a vcc_fqdn inside CbfMaster, for example:
            # vcc_id = 1 -> mid_csp_cbf/vcc/vcc_001
            # vcc_id = 2 -> mid_csp_cbf/vcc/vcc_002
            # .....
            # vcc_id = 17 -> mid_csp_cbf/vcc/vcc_017
            # vcc_id and receptor_id is not the same. The map between vcc_id and receptor_id
            # is built by CbfMaster and exported as attribute.
            # The max number of VCC allocated is defined by the VCC property of the CBF Master.

            device = self.target
            available_receptors = []
            # the list of receptor to assign to the subarray
            receptor_to_assign = []
            #receptors = Receptors(device.CspMaster)
            try:
                available_receptors = device._receptors.unassigned_ids()
                self.logger.info("available_receptors: {}".format(available_receptors))
                if not any(available_receptors):
                    log_msg = "No available receptor to add to subarray {}".format(device.SubID)
                    self.logger.info(log_msg)
                    return []
                # the list of available receptor IDs. This number is mantained by the CspMaster
                # and reported on request.
                receptor_id_list = device._receptors.list_of_ids()
                self.logger.info("receptor_list:{}".format(receptor_id_list))
                receptor_membership = device._receptors.subarray_affiliation()
            except tango.DevFailed as df:
                msg = "Failure in getting receptors information:" + \
                    str(df.args[0].reason)
                tango.Except.throw_exception("Command failed", msg,
                                         "AddReceptors", tango.ErrSeverity.ERR)
            for receptorId in argin:
                # check if the specified receptor id is a valid number (that is, belongs to the list
                # of provided receptors)
                if receptorId in receptor_id_list:
                    # check if the receptor id is one of the available receptor Ids
                    if receptorId in available_receptors:
                        receptor_to_assign.append(receptorId)
                    else:
                        # retrieve the subarray owner
                        sub_id = receptor_membership[receptorId - 1]
                        log_msg = "Receptor {} already assigned to subarray {}".format(str(receptorId),
                                                                                       str(sub_id))
                        self.logger.info(log_msg)
                else:
                    log_msg = "Invalid receptor id: {}".format(str(receptorId))
                    self.logger.warning(log_msg)

            # check if the list of receptors to assign is empty
            if not receptor_to_assign:
                log_msg = "No receptors to assign to the subarray"
                self.logger.info(log_msg)
                return []

            # remove possible receptor repetition
            tmp = set(receptor_to_assign)
            receptor_to_assign = list(tmp)
            return receptor_to_assign
            # PROTECTED REGION END #    //  MidCspSubarrayBase.AddReceptors

    class ReleaseResourcesCommand(CspSubarray.ReleaseResourcesCommand):

        def do(self, argin):
            """
            Stateless hook for ReleaseResources() command functionality.
            :param argin: The resources to be released.
            :type argin: string (JSON formatted)
            :return: A tuple containing a return code and a string
                message indicating status. The message is for
                information purpose only.
            :rtype: (ResultCode, str)
            """
            super().do(argin)
            try: 
                resources_dict = json.loads(argin)
                subarray_id = resources_dict['subarrayID']
                if subarray_id != int(self.target.SubID):
                    msg = f"Mismatch in subarrayID. Received: {subarray_id}"
                    self.logger.error(msg)
                    return (ResultCode.FAILED, msg)
                receptor_list = list(map(int, resources_dict['dish']['receptorIDList']))
            except (KeyError,  json.JSONDecodeError, Exception) as error:
                msg = f"Something wrong in Json input string: {error}"
                return (ResultCode.FAILED, msg)
            return self._remove_receptors(receptor_list)

        def _remove_receptors(self, argin):
            device = self.target
            # check if the CspSubarray is already connected to the CbfSubarray
            if not device._is_sc_subarray_running(device.CbfSubarray):
                log_msg = "Subarray {} not registered!".format( str(device.CbfSubarray))
                self.logger.error(log_msg)
                return (ResultCode.FAILED, log_msg)
            try:
                proxy = device._sc_subarray_proxies[device.CbfSubarray]
                # read from CbfSubarray the list of assigned receptors
                # failure in reading attribute raises an AttributeError (not tangoDevFailed!!)
                #receptors = proxy.receptors
                receptors = device._receptors.assigned_to_subarray(device.SubID)
                #!!!!!!!!!!!!!!!!!
                # 2019-09-20:  New images for TANGO and PyTango images has been released. PyTango
                # is now compiled with th numpy support. In this case the proxy.receptors call
                # does no more return an empty tuple but an empty numpy array.
                # Checks on receptors content need to be changed (see below)
                # NB: the receptors attribute implemented by the CbfSubarray is declared as RW.
                # In this case the read method returns an empty numpy array ([]) whose length is 0.
                #!!!!!!!!!!!!!!!!!

                # check if the list of assigned receptors is empty
                if len(receptors):
                    receptors_to_remove = []
                    # check if the receptors to remove belong to the subarray
                    for receptor_id in argin:
                        if receptor_id in receptors:
                            receptors_to_remove.append(receptor_id)
                    if any(receptors_to_remove):
                        # TODO: add reading of CbfSubarray removeReceptorsCmdDurationExpected
                        # attribute, if implemented, otherwise use the default value

                        # subscribes attributes to track progress and timeout: if these
                        # attributes are not implemented at CbfSubarray level, the warning
                        # is only logged because is not a fatal error.
                        for attr in ['removeReceptorsCmdProgress', 'timeoutExpiredFlag']:
                            try:
                                if device._sc_subarray_event_id[device.CbfSubarray][attr.lower()] == 0:
                                    evt_id = proxy.subscribe_event(attr, tango.EventType.CHANGE_EVENT,
                                                                   device._attributes_change_evt_cb, stateless=False)
                                    device._sc_subarray_event_id[device.CbfSubarray][attr.lower()] = evt_id
                            except tango.DevFailed as tango_err:
                                self.logger.debug(tango_err.args[0].desc)
                        device._timeout_expired = False
                        device._failure_raised = False
                        # forward the command to CbfSubarray
                        proxy.command_inout_asynch("RemoveReceptors", receptors_to_remove, device._cmd_ended_cb)
                        if device._cmd_execution_state['removereceptors'] != CmdExecState.FAILED:
                            device._cmd_execution_state['removereceptors'] = CmdExecState.RUNNING
                        device._sc_subarray_cmd_starting_time[device.CbfSubarray] = time.time()
                        # Note: rembember to put the comma in args=(receptors_to_remove,) otherwise
                        # the list is received as a numpy array!
                        device._command_thread['removereceptors'] = threading.Thread(target=device._monitor_remove_receptors,
                                                                                   name="Thread-RemoveReceptors",
                                                                                   args=(receptors_to_remove,))
                        device._command_thread['removereceptors'].start()
                    else : 
                        return (ResultCode.OK, f"No receptor to remove from subarray {device.get_name()}")
                device._cmd_execution_state['removereceptors'] = CmdExecState.IDLE
                return (ResultCode.STARTED, "RemoveReceptor started")
            except tango.DevFailed as tango_err:
                message = str(tango_err.args[0].desc)
            except AttributeError as attr_err:
                message = str(attr_err)
            return (ResultCode.FAILED, message)    

    class ReleaseAllResourcesCommand(SKASubarray.ReleaseAllResourcesCommand):
        def do(self):
            return self.remove_all_receptors()

        def remove_all_receptors(self):
            self.logger.info("Going to remove all receptors")
            device = self.target
            try: 
                if len(device):
                    receptors = device._receptors.assigned_to_subarray(device.SubID)
                    receptors = receptors.tolist()
                    self.logger.info("assigned receptors:{}".format(receptors))
                    release_dict = {}
                    release_dict["dish"] = {"receptorIDList":  receptors[:]}
                    release_dict['subarrayID'] = int(device.SubID)
                    self.logger.info(release_dict)
                    return device._releaseresources_cmd_obj.do(json.dumps(release_dict))
                return (ResultCode.OK, "No receptor to remove")
            except tango.DevFailed as df:
                log_msg = ("RemoveAllReceptors failure. Reason: {} "
                           "Desc: {}".format(df.args[0].reason,
                                             df.args[0].desc))
                self.logger.error(log_msg)
                return (ResultCode.FAILED, log_msg)    

    class ConfigureCommand(CspSubarray.ConfigureCommand):
        def do(self,argin):
            self.logger.info("Call Mid CspSubarray Configure")
            try:
               return super().do(argin)
            except tango.DevFailed as tango_err:
                log_msg = ("Configure Command failure. Reason: {} "
                           "Desc: {}".format(tango_err.args[0].reason,
                                             tango_err.args[0].desc))
                #tango.Except.throw_exception("Command failed",
                #                             log_msg,
                #                             "ConfigureScan execution",
                #                             tango.ErrSeverity.ERR)
                self.logger.error(log_msg)
                return (ResultCode.FAILED, log_msg)

        def validate_scan_configuration(self, argin):
            """
            Overwritten method.
            Validate the MID scan configuration file.
            Currently it only copies the received configuration because it does not
            still exist any "cbf" block inside the JSON script.
            :param json_config: the input JSON formatted string with the configuration
                                for a scan
            """

            '''
            TODO:
            - look for the sub-element entries
            - add eventual information to the sub-element blocks (for example for PSS add the
            addresses of the PSS pipelines or the IDs of the SearchBeams)
            - set the observing mode for each sub-element sub-array/PSTBeam
            json_config = {}
            try:
                json_config = json.loads(argin)                               
            except json.JSONDecodeError as e:  # argument not a valid JSON object
                # this is a fatal error
                msg = ("Scan configuration object is not a valid JSON object."
                       "Aborting configuration:{}".format(str(e)))
                self.logger.error(msg)
                tango.Except.throw_exception("Command failed",
                                             msg,
                                             "ConfigureScan execution",
                                             tango.ErrSeverity.ERR)
            if "cbf" in json_config:
                if self._sc_subarray_state[self.CbfSubarray] != tango.DevState.ON:
                    pass
                    # throw exception
                self._sc_subarray_obs_mode[self.CbfSubarray] = ObsMode.CORRELATION
                self._sc_scan_configuration[self.CbfSubarray] = json.dumps(json_config["cbf"])
                self._sc_subarray_assigned_fqdn.append(self.CbfSubarray)
            if "pss" in json_config:
                if self._sc_subarray_state[self.PssSubarray] != tango.DevState.ON:
                    pass
                    # throw exception
                #self._sc_subarray_obs_mode[self.PssSubarray] = ??
                self._sc_subarray_assigned_fqdn.append(self.PssSubarray)
                pss_json = json_config["pss"]
                if "searchbeams" not in self._sc_scan_configuration[self.PssSubarray]: 
                    pss_json["searchbeams"] = self._assigned_search_beams
                self._sc_scan_configuration[self.PssSubarray] = json.dumps(pss_json)

            if "pst" in json_config:
                ....
            '''
            self.logger.info("Mid CspSubarray validate_scan")
            device = self.target
            device._sc_subarray_assigned_fqdn.clear()
            device._sc_subarray_assigned_fqdn.append(device.CbfSubarray)
            json_dict = {}
            try:
                #CspSubarray.validate_scan_configuration(self, argin)
                super().validate_scan_configuration(argin)
                json_dict = json.loads(argin)
            except json.JSONDecodeError as e:  # argument not a valid JSON object
                # this is a fatal error
                msg = ("Scan configuration object is not a valid JSON object."
                       "Aborting configuration:{}".format(str(e)))
                self.logger.error(msg)
                tango.Except.throw_exception("Command failed",
                                             msg,
                                             "ConfigureScan execution",
                                             tango.ErrSeverity.ERR)
            # TODO:
            # validate some entries of json_dict
            self.logger.debug("Validate scan configuration for MID CSP")
            #self._sc_subarray_obs_mode[self.CbfSubarray] = ObsMode.IMAGING
            #self._sc_scan_configuration[self.CbfSubarray] = json_config["cbf"]
            json_config = JsonConfiguration(argin, self.logger)
            device._sc_subarray_scan_configuration[device.CbfSubarray] = json_config.build_json_cbf()
            self.logger.debug("Validate scan: {}".format(device._sc_subarray_assigned_fqdn))

    def __len__(self):
        assigned_receptors = self._receptors.assigned_to_subarray(int(self.SubID))
        self.logger.debug("len assigned_receptors:{}".format(assigned_receptors))
        return len(assigned_receptors)

    def init_command_objects(self):
        """
        Sets up the command objects
        """
        super().init_command_objects()

        args = (self, self.state_model, self.logger)

        self._assignresources_cmd_obj = self.AssignResourcesCommand(*args)
        self._releaseresources_cmd_obj = self.ReleaseResourcesCommand(*args)
        self.register_command_object("AssignResources", self.AssignResourcesCommand(*args))
        self.register_command_object("ReleaseResources", self.ReleaseResourcesCommand(*args))
        self.register_command_object("ReleaseAllResources", self.ReleaseAllResourcesCommand(*args))
   
    def update_subarray_state(self):
        """
        Class protected method.
        Retrieve the State attribute values of the CSP sub-elements and aggregate
        them to build up the CSP global state.

        :param: None

        :return: None
        """
        if CspSubarray.update_subarray_state(self):
            if self.get_state() == DevState.ON:
                if self._sc_subarray_obs_state[self.CbfSubarray] == ObsState.IDLE:
                    self._assignresources_cmd_obj.succeeded()
                if self._sc_subarray_obs_state[self.CbfSubarray] == ObsState.EMPTY:
                    self._releaseeceptors_cmd_obj.succeeded()
            self.logger.info("MidCsp subarray state: {} obsState: {}".format(self.get_state(), self.state_model._obs_state))
        return True

    def _open_connection(self, fqdn):
        device_proxy = DeviceProxy(fqdn)
        return device_proxy

    '''
    def _get_cbfsubarray_assigned_receptors(self):
        """

        :param device_proxy: DeviceProxy of the CBF Sub-element subarray
        :return: the list of receptors Id on success, otherwise []
        """
        receptors = []
        try:
            # Note: with numpy support in PyTango, receptors is a
            # numpy array
            device_proxy = self._sc_subarray_proxies[self.CbfSubarray]
            receptors = device_proxy.receptors
        except tango.DevFailed as tango_err:
            self.logger.warning("__monitor_add_receptors:{}".format(tango_err.args[0].desc))
        return receptors
    '''

    def _monitor_add_receptors(self, receptors_to_be_added, args_dict=None):
        cmd_name = 'addreceptors'
        device = self.CbfSubarray
        self._num_dev_completed_task[cmd_name] = 0
        self._list_dev_completed_task[cmd_name] = []
        self._cmd_progress[cmd_name] = 0
        self._cmd_duration_measured[cmd_name] = 0
        # sub-component command execution measured time
        self._sc_subarray_cmd_progress[device][cmd_name] = 0
        self._failure_message[cmd_name] = ''
        device_proxy = self._sc_subarray_proxies[device]
        self.logger.info("Going to assign receptors {}".format(receptors_to_be_added))

        while True:
            if self._sc_subarray_obs_state[device] == ObsState.IDLE:
                self.logger.info("Reconfiguring is:{}".format(self._reconfiguring))
                assigned_receptors = self._receptors.assigned_to_subarray(self.SubID)
                self.logger.info("assigned_receptors:{}".format(assigned_receptors))
                if not self._reconfiguring:
                    self._num_dev_completed_task[cmd_name] = len(assigned_receptors)
                    self.logger.info("All required receptors assigned!!")
                    self._sc_subarray_cmd_progress[device][cmd_name] = 100
                    # calculate the real execution time for the command
                    self._cmd_duration_measured[cmd_name] = (
                        time.time() - self._sc_subarray_cmd_starting_time[device])
                    break
            if self._sc_subarray_obs_state[device] == ObsState.RESOURCING:
                self._reconfiguring = False
                self.logger.info("Reconfiguring is:{}".format(self._reconfiguring))
            # check if sub-element command ended throwing an exception: in this case the
            # 'cmd_ended_cb' callback is invoked.
            if self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED or\
                    self._sc_subarray_obs_state[device] == ObsState.FAULT:
                self._failure_raised = True
                break
            elapsed_time = time.time() - \
                self._sc_subarray_cmd_starting_time[device]
            if (elapsed_time > self._sc_subarray_cmd_duration_expected[device][cmd_name] or
                    self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.TIMEOUT):
                self._timeout_expired = True
                msg = ("Timeout executing {} command  on device {}".format(cmd_name, device))
                self.logger.warning(msg)
                self._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.TIMEOUT
                break
            # update the progress counter inside the loop taking into account the number of devices
            # executing the command
            self._cmd_progress[cmd_name] = self._sc_subarray_cmd_progress[device][cmd_name]
            time.sleep(0.1)
        # end of the while loop

        self._last_executed_command = cmd_name
        # update the progress counter at the end of the loop
        self._cmd_progress[cmd_name] = self._sc_subarray_cmd_progress[device][cmd_name]
        self._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
        self._cmd_execution_state[cmd_name] = CmdExecState.IDLE
        # check for error conditions
        self.logger.info("CspSubarray failure flag:{}".format(self._failure_raised))
        self.logger.info("CspSubarray timeout flag:{}".format(self._timeout_expired))
        if self._timeout_expired or self._failure_raised:
            return self._assignresources_cmd_obj.failed()
        # reset the command execution state
        self.logger.info("AssignResources ends with success")
        return self._assignresources_cmd_obj.succeeded()

     # PROTECTED REGION END #    //  MidCspSubarrayBase.private_methods

    def _monitor_remove_receptors(self, receptor_list, args_dict=None):
        cmd_name = 'removereceptors'
        device = self.CbfSubarray
        self._num_dev_completed_task[cmd_name] = 0
        self._list_dev_completed_task[cmd_name] = []
        self._cmd_progress[cmd_name] = 0
        self._cmd_duration_measured[cmd_name] = 0
        # sub-component command execution measured time
        self._sc_subarray_cmd_progress[device][cmd_name] = 0
        self._failure_message[cmd_name] = ''
        device_proxy = self._sc_subarray_proxies[device]
        while True:
            # read the list of receptor IDs assigned to the CbfSubarray
            try:
                receptors = device_proxy.receptors
            except AttributeError as attr_err:
                self.logger.warning("RemoveReceptors:{}".format(str(attr_err)))
            if len(receptors):
                # get the ids in receptor_list that are no more present in receptors
                receptors_removed = [
                    value for value in receptor_list if value not in receptors]
                self._num_dev_completed_task[cmd_name] = len(
                    receptors) - len(receptor_list)
                if len(receptors_removed) == len(receptor_list):
                    self._sc_subarray_cmd_progress[device][cmd_name] = 100
                    # calculate the real execution time for the command
                    self._cmd_duration_measured[cmd_name] = (
                        time.time() - self._sc_subarray_cmd_starting_time[device])
                    self.logger.info(
                        "Receptors {} have been successfully removed".format(receptor_list))
                    break
            else:
                self.logger.info("Receptors already successfully removed")
                self._sc_subarray_cmd_progress[device][cmd_name] = 100
                break
            # check if sub-element command ended throwing an exception: in this case the
            # 'cmd_ended_cb' callback is invoked.
            if self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED:
                break
            elapsed_time = time.time() - \
                self._sc_subarray_cmd_starting_time[device]
            if (elapsed_time > self._sc_subarray_cmd_duration_expected[device][cmd_name] or
                    self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.TIMEOUT):
                msg = ("Timeout executing {} command  on device {}".format(
                    cmd_name, device))
                self.logger.warning(msg)
                self._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.TIMEOUT
                break
            # update the progress counter inside the loop taking into account the number of devices
            # executing the command
            self._cmd_progress[cmd_name] = self._sc_subarray_cmd_progress[device][cmd_name]
            time.sleep(0.1)
        # end of the while loop
        self._last_executed_command = cmd_name
        # update the progress counter at the end of the loop
        self._cmd_progress[cmd_name] = self._sc_subarray_cmd_progress[device][cmd_name]
        # check for error conditions
        if self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.FAILED:
            self._failure_raised = True
        if self._sc_subarray_cmd_exec_state[device][cmd_name] == CmdExecState.TIMEOUT:
            self._timeout_expired = True
        if self._sc_subarray_state[self.CbfSubarray] not in [tango.DevState.OFF,
                                                             tango.DevState.ON]:
            self._failure_message[cmd_name] += ("Device {} is in {} State".format(device,
                                                                                self._sc_subarray_state[self.CbfSubarray]))
            self.logger.warning(self._failure_message[device])
            self._failure_raised = True
            self.logger.error("ReleaseReceptors ended with failure")
        self.logger.info("CspSubarray failure flag:{}".format(self._failure_raised))
        self.logger.info("CspSubarray timeout flag:{}".format(self._timeout_expired))
        self._sc_subarray_cmd_exec_state[device][cmd_name] = CmdExecState.IDLE
        self._cmd_execution_state[cmd_name] = CmdExecState.IDLE
        if self._failure_raised  or self._timeout_expired:
            return self._releaseresources_cmd_obj.failed()
        # reset the command exeuction state
        self.logger.info("Receptors removed with success")
        return self._releaseresources_cmd_obj.succeeded()

    #@command(
    #    dtype_in='DevString',
    #    doc_in="A Json-encoded string with the scan configuration.",
    #)
    #@DebugIt()
    #def Configure(self, argin):
    #    CspSubarray.Configure(self, argin)

    # PROTECTED REGION END #    //  MidCspSubarrayBase.private_methods
    # ----------------
    # Class Properties
    # ----------------

    # -----------------
    # Device Properties
    # -----------------
    # ----------
    # Attributes
    # ----------
    assignedFsp = attribute(
        dtype=('DevUShort',),
        max_dim_x=27,
        label="List of assigned FSPs",
        doc="List of assigned FSPs.",
    )

    assignedVcc = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="List of assigned VCCs",
        doc="List of assigned VCCs.",
    )

    addReceptorsCmdProgress = attribute(
        dtype='DevUShort',
        label="AddReceptors command progress percentage",
        polling_period=1500,
        doc="The progress percentage for the AddReceptors command.",
    )

    removeReceptorsCmdProgress = attribute(
        dtype='DevUShort',
        label="RemoveReceptors command progress percentage",
        polling_period=1500,
        doc="The progress percentage for the RemoveReceptors command.",
    )

    assignedVccState = attribute(
        dtype=('DevState',),
        max_dim_x=197,
        label="assignedVccState",
        doc="The State of the assigned VCCs.",
    )

    assignedVccHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="assignedVccHealthState",
        doc="The health state of the assigned VCCs.",
    )

    assignedVccObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="assignedVccObsState",
        doc="The observing state of the assigned VCCs.",
    )

    assignedVccAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=197,
        label="assignedVccAdminMode",
        doc="The admin mode of the assigned VCCs.",
    )

    assignedFspState = attribute(
        dtype=('DevState',),
        max_dim_x=197,
        label="assignedFSPState",
        doc="The State of the assigned FSPs.",
    )

    assignedFspHealthState = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="assignedFSPHealthState",
        doc="The health state of the assigned FSPs.",
    )

    assignedFspObsState = attribute(
        dtype=('DevUShort',),
        max_dim_x=197,
        label="assignedFspObsState",
        doc="The observing state of the assigned FSPs.",
    )

    assignedFspAdminMode = attribute(
        dtype=('DevUShort',),
        access=AttrWriteType.READ_WRITE,
        max_dim_x=197,
        label="assignedFspAdminMode",
        doc="The admin mode of the assigned FSPs.",
    )

    assignedReceptors = attribute(
        dtype=('DevULong',),
        max_dim_x=197,
        label="assignedReceptors",
        doc="The list of receptors assigned to the subarray.",
    )

    # ---------------
    # General methods
    # ---------------

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspSubarrayBase.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayBase.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspSubarrayBase.delete_device) ENABLED START #
        CspSubarray.delete_device(self)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.delete_device
    # ------------------
    # Attributes methods
    # ------------------
    def read_addReceptorsCmdProgress(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.addReceptorsCmdProgress_read) ENABLED START #
        """Return the addReceptorsCmdProgress attribute."""
        return self._cmd_progress['addreceptors']
        # PROTECTED REGION END #    //  MidCspSubarrayBase.addReceptorsCmdProgress_read

    def read_removeReceptorsCmdProgress(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.removeReceptorsCmdProgress_read) ENABLED START #
        """Return the removeReceptorsCmdProgress attribute."""
        return self._cmd_progress['removereceptors']
        # PROTECTED REGION END #    //  MidCspSubarrayBase.removeReceptorsCmdProgress_read
        
    def read_assignedFsp(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFsp_read) ENABLED START #
        """Return the assignedFsp attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFsp_read

    def read_assignedVcc(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVcc_read) ENABLED START #
        """
        *Attribute method*

        :returns:
            The list of VCC IDs assigned to the subarray.

            *Type*: array of DevUShort.
        """
        # PROTECTED REGION ID(CspSubarray.vcc_read) ENABLED START #
        self._assigned_vcc.clear()
        # get the map VCC-receptor from CspMaster
        #receptor_id_list = self._receptors.list_of_ids()
        '''
        if not self._receptor_to_vcc_map:
            try:
                csp_proxy = tango.DeviceProxy(self.CspMaster)
                csp_proxy.ping()
                cbf_master_addr = csp_proxy.cbfMasterAddress
                cbf_master_proxy = tango.DeviceProxy(cbf_master_addr)
                cbf_master_proxy.ping()
                # build the list of receptor ids
                receptor_to_vcc = cbf_master_proxy.receptorToVcc
                self._receptor_to_vcc_map = dict([int(ID) for ID in pair.split(":")]
                                                 for pair in receptor_to_vcc)
                # build the list of the installed receptors
                self._receptor_id_list = list(self._receptor_to_vcc_map.keys())
            except tango.DevFailed as tango_err:
                self.logger.warning(tango_err.args[0].desc)
        '''
        try:
            assigned_receptors = self._receptors.assigned_to_subarray(self.SubID)
            # NOTE: if receptors attribute is empty, assigned_receptors is an empty numpy array
            # and it will just be skipped by the for loop
            receptor_to_vcc_map = self._receptors.receptor_vcc_map()
            for receptor_id in assigned_receptors:
                vcc_id = receptor_to_vcc_map[receptor_id]
                self._assigned_vcc.append(vcc_id)
        except KeyError as key_err:
            msg = "No {} found".format(key_err)
            tango.Except.throw_exception("Read attribute failure",
                                         msg,
                                         "read_vcc",
                                         tango.ErrSeverity.ERR)
        except tango.DevFailed as df:
            tango.Except.throw_exception("Read attribute failure",
                                         df.args[0].desc,
                                         "read_vcc",
                                         tango.ErrSeverity.ERR)
        return self._assigned_vcc
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedVcc_read

    def read_assignedVccState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVccState_read) ENABLED START #
        """Return the assignedVccState attribute."""
        return (tango.DevState.UNKNOWN,)
    def read_assignedVccHealthState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVccHealthState_read) ENABLED START #
        """Return the assignedVccHealthState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedVccHealthState_read

    def read_assignedVccObsState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVccObsState_read) ENABLED START #
        """Return the assignedVccObsState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedVccObsState_read

    def read_assignedVccAdminMode(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVccAdminMode_read) ENABLED START #
        """Return the assignedVccAdminMode attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedVccAdminMode_read

    def write_assignedVccAdminMode(self, value):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedVccAdminMode_write) ENABLED START #
        """Set the assignedVccAdminMode attribute."""
        pass
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedVccAdminMode_write

    def read_assignedFspState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFspState_read) ENABLED START #
        """Return the assignedFspState attribute."""
        return (tango.DevState.UNKNOWN,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFspState_read

    def read_assignedFspHealthState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFspHealthState_read) ENABLED START #
        """Return the assignedFspHealthState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFspHealthState_read

    def read_assignedFspObsState(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFspObsState_read) ENABLED START #
        """Return the assignedFspObsState attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFspObsState_read

    def read_assignedFspAdminMode(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFspAdminMode_read) ENABLED START #
        """Return the assignedFspAdminMode attribute."""
        return (0,)
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFspAdminMode_read

    def write_assignedFspAdminMode(self, value):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedFspAdminMode_write) ENABLED START #
        """Set the assignedFspAdminMode attribute."""
        pass
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedFspAdminMode_write

    def read_assignedReceptors(self):
        # PROTECTED REGION ID(MidCspSubarrayBase.assignedReceptors_read) ENABLED START #
        """Return the assignedReceptors attribute."""
        assigned_receptors = self._receptors.assigned_to_subarray(self.SubID)
        if len(assigned_receptors) == 0:
            return [0]
        return assigned_receptors
        # PROTECTED REGION END #    //  MidCspSubarrayBase.assignedReceptors_read

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(MidCspSubarrayBase.main) ENABLED START #
    return run((MidCspSubarrayBase,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspSubarrayBase.main


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-
#
# This file is part of the MidCspSubarrayProcModeVlbi project
#
# INAF-SKA Telescope
#
# Distributed under the terms of the GPL license.
# See LICENSE.txt for more info.

""" MID CSP Subarray Processing Mode Correlation Capability

The class exposes parameters nd commands required for monitor and
control of the Processing Mode VLBI for a single MID Csp Subarray.
"""
# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango.server import device_property
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
from csp_lmc_common.CspSubarrayProcModeVlbi import CspSubarrayProcModeVlbi
# Additional import
# PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.additionnal_import) ENABLED START #
# PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.additionnal_import

__all__ = ["MidCspSubarrayProcModeVlbi", "main"]


class MidCspSubarrayProcModeVlbi(CspSubarrayProcModeVlbi):
    """
    The class exposes parameters nd commands required for monitor and
    control of the Processing Mode VLBI for a single MID Csp Subarray.

    **Properties:**

    - Device Property
    """
    # PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.class_variable

    # -----------------
    # Device Properties
    # -----------------








    # ----------
    # Attributes
    # ----------

















    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initialises the attributes and properties of the MidCspSubarrayProcModeVlbi."""
        CspSubarrayProcModeVlbi.init_device(self)
        # PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.init_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.delete_device) ENABLED START #
        # PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.delete_device
    # ------------------
    # Attributes methods
    # ------------------


    # --------
    # Commands
    # --------

# ----------
# Run server
# ----------


def main(args=None, **kwargs):
    # PROTECTED REGION ID(MidCspSubarrayProcModeVlbi.main) ENABLED START #
    return run((MidCspSubarrayProcModeVlbi,), args=args, **kwargs)
    # PROTECTED REGION END #    //  MidCspSubarrayProcModeVlbi.main

if __name__ == '__main__':
    main()

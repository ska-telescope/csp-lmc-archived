# -*- coding: utf-8 -*-
#
# This file is part of the CentralNode project
#
#
#
# Distributed under the terms of the BSD-3-Clause license.
# See LICENSE.txt for more info.

"""Release information for Python Package"""

name = """mid-csp-lmc"""
version = "0.7.5"
version_info = version.split(".")
description = """SKA MID CSP.LMC"""
author = "INAF-OAA"
author_email = "elisabetta.giani@inaf.it"
license = """BSD-3-Clause"""
url = """https://gitlab.com/ska-telescope/csp-lmc.git"""
copyright = """INAF, SKA Telescope"""

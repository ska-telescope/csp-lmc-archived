.. Documentation

####################
Mid CSP.LMC Subarray
####################
.. |br| raw:: html

   <br />

CSP_Mid signal processing configuration and control is via sub-arrays and scans.|br|
The CSP_Mid sub-array receives configuration per scan. |br|
The following CSP_Mid sub-array parameters must be set in advance of a scan (before the
command  *Configure* is received):

* Receptors that belong to sub-arrays. In order for a CSP_Mid sub-array to accept a scan configuration,
  at least one receptor must be assigned to the sub-arrays. If no receptors are assigned to a sub-array, the sub-array is 
  empty and its state is *OFF*.
  
* Tied-array beams to be used by the sub-array. Search Beams, Timing Beams and Vlbi Beams are assigned via the CSP_Mid sub-array.
  TM may specify the number of beams to be used or, alternatively, identify the Capability instances to be used
  via their TANGO Fully Qualified Domain Name (FQDN). Search Beams must be assigned in advance of a scan that requires
  PSS processing. The same for Timing and Vlbi Beams.

Receptors assignment
=====================
The assignment of receptors to a MID subarray is performed in advance of a scan configuration. |br|
Up to 197 receptors can be assigned to one sub-array. |br|
Receptors assignment to a subarray is exclusive: one receptor can be assigned only to one subarray. |br|
During assignement, the  CSP_Mid subarray checks if the requested receptor is already assigned
to another sub-arry, and in this case it logs a warning message.

Inherent Capabilities
---------------------
Each Mid CSP.LMC subarray has also four permanently assigned *inherent Capabilities*: 

* Correlation
* PSS
* PST
* VLBI

An inherent Capability can be enabled or disabled, but cannot assigned or removed to/from a subarray. 
They correspond to the CSP Mid Processing Modes and are configured via a scan configuration.

Scan configuration
====================

TM provides a complete scan configuration to a subarray via an ASCII JSON encoded string.
Parameters specified via a JSON string are implemented as TANGO Device attributes  
and can be accessed and modified directly using the buil-in TANGO method *write_attribute*.
When a complete and coherent scan configuration is received and the subarray configuration 
(or re-configuration) completed,  the subarray it's ready to observe.

Control and Monitoring
======================
Each CSP Subarray maintains and report the status and state transitions for the 
CSP subarray as a whole and for the individual assigned resources.

In addition to pre-configured status reporting, a CSP Subarray makes provision for the TM and any authorized client, 
to obtain the value of any subarray attribute.

MidCspSubarray Class Documentation 
==================================

.. autoclass:: csp_lmc_mid.MidCspSubarrayBase.MidCspSubarrayBase
   :members:
   :undoc-members:
   :show-inheritance:
   :member-order:
   :noindex:

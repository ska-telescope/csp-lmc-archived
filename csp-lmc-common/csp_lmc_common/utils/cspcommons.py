from enum import IntEnum, unique

@unique
class CmdExecState(IntEnum):
    IDLE = 0
    RUNNING = 1
    QUEUED = 2
    TIMEOUT = 3
    FAILED = 4

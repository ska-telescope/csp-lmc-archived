import json
from ska_telmodel.csp.schema import validate_csp_config 
from ska_telmodel.csp.version import validate_csp_config_version

class JsonConfiguration:
    """
    A set of methods to manage the json configuration inside CSP code
    """
    def __init__(self, json_config:str, logger):
        self.major_version = None
        self.minor_version = None
        self.logger = logger
        self.config = json.loads(json_config)

    def detect_version(self):
        """
        Detect the version of the json configuration
        """
        #ADR-22
        if 'interface' in self.config.keys():
            interface = self.config['interface']
            if validate_csp_config_version(interface):
                version_str = interface.split('/')[-1]
                self.major_version , self.minor_version = map(int, version_str.split('.'))
            else:
                message = 'Wrong json schema URI!'
                self.logger.error(message)
                raise ValueError(message)
        #ADR-18
        elif 'common' in self.config.keys():
            self.major_version, self.minor_version = (1,0)
        #pre ADR-18
        else: 
            self.major_version, self.minor_version = (0,1)
        self.version = (self.major_version, self.minor_version)
        self.logger.info(f'Version is{self.major_version}{self.minor_version}')

    def get_section(self, name:str):
        """
        Returns the correspondant section of json configuration.
        """
        try:
            return self.config[name]
        except KeyError as e:
            self.logger.error(e)
            raise ValueError(e) from e

    def get_id(self):
        """
        Return the json configuration id
        """
        try:
            common_sec = self.get_section('common')
            config_id = common_sec['id']
        except ValueError as e:
            if 'id' in self.config:
                config_id = self.config['id']
            else:
                raise ValueError(e) from e
        return config_id

    def conversion_10_01(self):
        """
        Converts the json from version 1.0 (ADR-18) to version 0.1.
        Note: Current Mid.CBF supports 0.1 Json schema.
        """
        common_dict = self.get_section('common')
        cbf_dict = self.get_section('cbf')
        if 'transaction_id' in self.config:
            common_dict['transaction_id'] = self.get_section('transaction_id')
        config_converted = {**common_dict, **cbf_dict}
        validate_csp_config(version=0, config=config_converted) 
        self.config = config_converted
            
    def build_json_cbf(self) -> str:
        """
        Returns the json configuration dictionary to be passed to cbf.
        If a version 0.2 is given, it converts to 0.1
        """
        self.detect_version()
        if self.version == (0, 1):
            pass
        elif self.version == (1, 0):
            self.conversion_10_01()
        else:
            raise ValueError(f'version{self.version}is not supported')
        return json.dumps(self.config)

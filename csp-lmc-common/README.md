## Description
General requirements for the monitor and control functionality are the same for both SKA Mid and Low 
telescopes.<br>
Two of three `CSP` Sub-elements, `PSS` and `PST`, have the same functionality and use the
same design in both telesopes.<br>
Functionality common to Low and Mid `CSP.LMC` includes:

* communication software
* logging
* archiving
* alarm generation
* sub-arraying
* some of the functionality related to handling observing mode
* Pulsar Search and Pulsar Timing.

The main differences between `Low` and `Mid` `CSP.LMC` are due to the different receivers and `CBF`
functionality and design.<br>
The `CSP.LMC Common Software` package provides the base software to develop the Low and Mid CSP.LMC.<br>
The `CSP.LMC` software is based on the `TANGO Control System`,the `SKA Control System Guidelines` and the `SKA CSP.LMC Base Classes`.
